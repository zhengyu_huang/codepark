import numpy as np
import FluidGeo
import sys


def AirfoilGmsh(fileNames, cl, layer_n, splineOrNot = True, skiprows = 0, z =.0):
    '''
    Data has two part upper and lower points
    :param fileNames: 
    Case 1: 2 columns
    It contains upper points from leading edge to trailing edge, and lower points
    from leading edge to trailing edge, 
    Case 2: 3 columes:
    It contains, x ; lower part from the leading edge to the trailing edge; upper part from the leading edge to the trailing edge 
    :param skiprows:
    :param plotOrNot:
    :return:
    save the points in airfoil:  trailing edge --- upper part --- leading edge --- lower part
    '''
    datum = np.loadtxt(fileNames, skiprows=skiprows)

    n = len(datum) // 2
    airfoil = np.zeros([2*n-2, 2])
    for i in range(n):
        airfoil[i, :] = datum[n - i - 1, 0:2]
    for i in range(n, 2*n - 2):
        airfoil[i, :] = datum[i + 1, 0:2]

    nAirfoil = len(airfoil)


    ########################################
    # Write Embedded Surface
    #######################################

    file = open('airfoil.geo','w')

    file.write('cl = %.12f;\n' %(cl));
    for i in range(nAirfoil):
        file.write('Point(%d) = { %.12f,  %.12f,  %.12f, cl};\n'%(i + 1, airfoil[i][0], airfoil[i][1], z))

    if(splineOrNot):
        #Upper surface
        file.write('Spline(1) = {')
        file.write(','.join(str(x) for x in range(1, n+1)))
        file.write('};\n')

        #Lower surface
        file.write('Spline(2) = {')
        file.write(','.join(str(x) for x in range(n, 2*n - 1, 1)))
        file.write(', 1};\n')

        #extrude lines
        file.write('ext = %.12f;\n' %(layer_n*cl))

        file.write('Extrude {0, 0, ext} {Line{1, 2}; Layers{%d};}\n' %layer_n)

        #write physical name
        file.write('Physical Surface(\"StickFixedSurface\") = {6,10};')
    else:

        # Upper surface
        for i in range(1, nAirfoil):
            file.write('Line(%d) = {%d,  %d};\n'%(i, i, i + 1))
        file.write('Line(%d) = {%d,  %d};\n'%(nAirfoil, nAirfoil, 1))



        # extrude lines
        file.write('ext = %.12f;\n' % (layer_n * cl))

        file.write('foil[] = Extrude {0, 0, ext} {Line{1 : %d}; Layers{%d};};\n' %(nAirfoil, layer_n))

        # write physical name
        file.write('Physical Surface(\"StickFixedSurface\") = {%d:%d:4};\n' %(nAirfoil + 4, nAirfoil*5))


    file.close()



def read_nodes(mshfile, z = 0.0):
    '''
    Read 3D airfoil top file
    :return:
    '''
    try:
        fid = open(mshfile, "r")
    except IOError:
        print("File '%s' not found." % mshfile)
        sys.exit()

    nodes = []
    print('Reading mesh ...')

    line = fid.readline()  # should be "Nodes FluidNodes"

    while line:
        line = fid.readline()
        data = line.split()
        if data[0] == 'Elements':
            break;
        nodes.append(list(map(float, data[1:4])))

    fid.close()
    nodes = np.array(nodes, dtype=float)

    eps = 1.0e-8

    airfoilNodes = []
    for i in range(len(nodes)):
        if(np.fabs(nodes[i,2] - z) < eps): # on z plane
            airfoilNodes.append(nodes[i,:])
    nodes = np.array(airfoilNodes, dtype=float)





    print('leading edge, trailing edge', nodes[0, :], nodes[1, :])
    upperAirfoil = [nodes[0, :], nodes[1,:]]
    lowerAirfoil = [nodes[0, :], nodes[1,:]]

    id = 2
    while id < len(nodes):
        upperAirfoil.append(nodes[id, :])
        if(nodes[id + 1,0]  < nodes[id,0]):      # y > -eps
            id += 1

        else:
            id += 1
            break
    while id < len(nodes):
        lowerAirfoil.append(nodes[id,:])
        id = id + 1

    upperAirfoil.sort(key = lambda x: x[0])
    lowerAirfoil.sort(key =lambda x: x[0])

    upperAirfoil = np.array(upperAirfoil, dtype=float)

    lowerAirfoil = np.array(lowerAirfoil, dtype=float)

    print("upperAirfoil start ", upperAirfoil[0, 0], upperAirfoil[0,1], upperAirfoil[0,2],
          "upperAirfoil end ", upperAirfoil[-1, 0], upperAirfoil[-1, 1], upperAirfoil[-1, 2])
    print("lowerAirfoil start ", lowerAirfoil[0, 0], lowerAirfoil[0,1], lowerAirfoil[0,2],
          "lowerAirfoil end ", lowerAirfoil[-1, 0], lowerAirfoil[-1, 1], lowerAirfoil[-1, 2])

    n1, n2 = len(upperAirfoil),len(lowerAirfoil)
    airfoil = np.zeros([n1 + n2 - 2, 2])
    for i in range(n1):
        airfoil[i, :] = upperAirfoil[n1 - i - 1, 0:2]
    for i in range(n1, n1 + n2 - 2):
        airfoil[i, :] = lowerAirfoil[i - n1 + 1, 0:2]


    return airfoil


def fluidDomainGeo(fileNames, cl_layer, layer_n):
    '''
    Data has two part upper and lower points
    :param fileNames: top file of the 3-D airfoil
    :return:
    '''
    airfoil = read_nodes(fileNames)
    nAirfoil = len(airfoil)


    def node_type_array(airfoil):
        nAirfoil = len(airfoil)
        fine, coarse = [], []

        for i in range(nAirfoil):
            if (airfoil[i, 0] > 0.9 or airfoil[i, 0] < 0.1):
                fine.append(i)
            else:
                coarse.append(i)
        return fine, coarse


    ########################################
    # Write fluid domain  Surface
    #######################################

    # Background
    x_bg_ball = 0.0
    y_bg_ball = 0.0
    r_bg_ball = 50.0
    cl_bg = 5

    # refinement ball
    x_ref_ball = 0.5
    y_ref_ball = 0.0
    r_ref_ball = 1.2
    cl_ref_ball = 0.005
    d_min_ball = r_ref_ball
    d_max_ball = r_bg_ball

    # refine birdwing
    # 1:lead edge + trailing edge,  2:top and bottom smooth parts
    # it has cl and dist_min , dist_max
    cl_ref_1 = 0.0001  # [0.0005,0.0005,0.0005,0.01];

    cl_ref_2 = 0.0001
    d_min_airfoil_1 = 7 * cl_ref_1
    d_min_airfoil_2 = d_min_airfoil_1
    disp = 4.5 * cl_ref_1

    d_max_airfoil = r_bg_ball



    domain_geo = 'domain.geo'
    file = open(domain_geo, 'w')
    file.write('cl_ref_1 = %.15f;\n' % (cl_ref_1))
    file.write('cl_ref_2 = %.15f;\n' % (cl_ref_2))
    file.write('cl_bg = %.15f;\n' % (cl_bg))
    file.write('cl_ref_ball = %.15f;\n' % (cl_ref_ball))
    file.write('cl_layer = %.15f;\n' % (cl_layer))
    file.write('layer_n = %d;\n' % (layer_n))
    file.write('r_ref_ball = %.15f;\n' % (r_ref_ball))

    point_id = 1


    outwardNorm = np.zeros((nAirfoil, 2))
    # find outward normal
    for i in range(nAirfoil):
        n1 = i
        n2 = 1 if i == nAirfoil - 1 else i + 1

        tn = airfoil[n2, :] - airfoil[n1, :]
        nn = np.array([tn[1], -tn[0]])

        outwardNorm[n1, :] += nn / np.linalg.norm(nn)
        outwardNorm[n2, :] += nn / np.linalg.norm(nn)

    for i in range(nAirfoil):
        outwardNorm[i, :] = outwardNorm[i, :] / np.linalg.norm(outwardNorm[i, :])

    # point for field 1,
    for i in range(nAirfoil):
        # disturb = (np.random.uniform(0., 1., 2) - 0.5) * min(cl_ref_1, cl_ref_2) / 4.0  # randomize the points location

        disturb = outwardNorm[i, :] * disp

        point_id = FluidGeo.writeElem(file, 'Point', point_id,
                                      [airfoil[i, 0] + disturb[0], airfoil[i, 1] + disturb[1], 0.0, 'cl_ref_2'])

    # point for field 2, ball
    point_id = FluidGeo.writeElem(file, 'Point', point_id, [x_ref_ball, y_ref_ball, 0.0, 'cl_ref_ball'])

    fine, coarse = node_type_array(airfoil)
    FluidGeo.writeMeshSize(file, 'Attractor', 1, [nAirfoil + 1])  # ball attractor
    FluidGeo.writeMeshSize(file, 'Attractor', 2, fine)  # fine part
    FluidGeo.writeMeshSize(file, 'Attractor', 3, coarse)  # coarse part

    # file, type, field_id, IField, LcMin, LcMax, DistMin, DistMax, Sigmoid
    FluidGeo.writeMeshSize(file, 'Threshold', 4, 1, 'cl_ref_ball', 'cl_bg', d_min_ball, d_max_ball, 0)  # ball
    FluidGeo.writeMeshSize(file, 'Threshold', 5, 2, 'cl_ref_1', 'cl_bg', d_min_airfoil_1, d_max_airfoil,
                           0)  # airfoil
    FluidGeo.writeMeshSize(file, 'Threshold', 6, 3, 'cl_ref_2', 'cl_bg', d_min_airfoil_2, d_max_airfoil,
                           0)  # airfoil
    FluidGeo.writeMeshSize(file, 'Min', 7, [4, 5, 6])

    # Background Mesh, the domain is a circle
    FluidGeo.backgroundMesh(file, 'circle', x_bg_ball, y_bg_ball, r_bg_ball, 0.0, layer_n, point_id)
    file.close()


if __name__ == "__main__":
    cl = 0.0005
    layer_n = 1

    AirfoilGmsh('RAE2822', cl, layer_n)

    #fluidDomainGeo('airfoil.top', cl, layer_n)
