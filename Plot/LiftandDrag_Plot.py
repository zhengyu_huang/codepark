import numpy as np
import matplotlib.pyplot as plt


#############  THIS IS FOR FSI SUSPENSION LINES
def LiftandDrag(fileNames, legendNames, symbols, skip_n = 1, scale = 1.0):
    n = len(fileNames);
    data = []
    for i in range(n):
        datum = np.loadtxt(fileNames[i], skiprows=0)
        data.append(datum);


    plt.figure(1)
    for i in range(n):
        plt.plot(data[i][::skip_n,1],data[i][::skip_n,4]/scale, symbols[i], markersize=5, linewidth=2, label = legendNames[i])
    
    plt.legend(loc=4)
    plt.grid()
    plt.xlabel('Time (s)',size=30, labelpad=21)
    plt.ylabel('Drag (kN)',size=30, labelpad=21)
    plt.tick_params(axis='both', labelsize=28)
    plt.tick_params(axis='both', labelsize=28)
    #plt.ticklabel_format(axis='both', style='sci', scilimits=(-2,2))

    plt.legend(loc=0,prop ={'size':31})

    plt.figure(2)
    for i in range(n):
        plt.plot(data[i][::skip_n,1],data[i][::skip_n,5]/scale, symbols[i], markersize=5, linewidth=2, label = legendNames[i])
    
    plt.legend(loc=4)
    plt.grid()
    plt.xlabel('Time (s)',size=30, labelpad=21)
    plt.ylabel('Lift (N)',size=30, labelpad=21)
    plt.tick_params(axis='both', labelsize=28)
    plt.tick_params(axis='both', labelsize=28)

    plt.legend(loc=0,prop ={'size':31})

    plt.show()


if __name__ == "__main__":
    #LiftandDrag(['../simulations/results.1/LiftandDrag','../../PID3D.0.1/simulations/results.1/LiftandDrag.out','../../PID3D.0.3/simulations/results.1/LiftandDrag.out'], ['h/4', 'h/2', 'h'], ['-o','-o','-o'], 100, 1)
    LiftandDrag(['../../../Scratch/AMRTests/PID3D.3/simulations_Smagorinsky_1/postpro.1/LiftandDrag.out'], ['Total Drag'], ['-o'], 100, 1000)
