import sys, os
import matplotlib.pyplot as plt
import numpy as np

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("__file__"))) + '/apps')
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("__file__"))) + '/src')

from Solver import *
from Navier_Stokes_Nozzle import *

def compute_A_dA(xx, L):
    H = 1.0      # tube width
    h = 6./100   # hole size
    T = 0.16      # hole thickness
    A = 1 - (1 - h)*np.cos(np.pi*(xx - 0.5*L)/T)**2
    A[xx < (L - T)/2.0] = 1.
    A[xx > (L + T)/2.0] = 1.

    dA = 2 * (1 - h) * np.sin(np.pi * (xx - 0.5 * L) / T) * np.cos(np.pi * (xx - 0.5 * L) / T) * (np.pi / T)
    dA[xx < (L - T) / 2.0] = 0.
    dA[xx > (L + T) / 2.0] = 0.

    return A*H, dA*H



if __name__ == '__main__':
    para = {}
    para['temporal_scheme'] = 'RK2' # ForwardEuler or RK2
    para['inv_spatial_scheme'] = 'Linear' # Linear or Constant

    para['application'] = 'Navier_Stokes_Nozzle'
    mu = 0.012707143364911851

    gam, R, Pr = 1.33, 1., 0.72
    eos = [gam, R, Pr]


    para['L'] = L = 4. # domain size
    para['num_elem'] = n = 800# number of elements

    dx = L/n


    xx_face = np.linspace(0, L, n + 1)
    xx = (xx_face[1:] + xx_face[0:n])/2
    A, dA = compute_A_dA(xx, L)
    A_face, dA_face = compute_A_dA(xx_face, L)


    plt.plot(xx, dA/A, label = 'dA/A')
    plt.plot(xx, A, label= 'A')
    plt.legend()
    plt.show()
    # exit()


    para['time'] = 1.99 # simulate time

    para['cfl'] = 0.4 # cfl number

    gam, R, Pr = 1.33, 1., 0.72

    para['boundary_condition'] = ['flow', 'flow'] # periodic, wall, or inflow
    para['output_dt'] = 0.4 # fixed time to output

    para['output_file'] = 'bin/Navier_Stokes_Nozzle'




    eos = [gam, R, Pr]
    par = [False, 1, mu, eos]  # viscous or not; source term type; viscosity
    box = Box(n, dx)
    app = Navier_Stokes_Nozzle(par, box, A, A_face, dA)


    solver = Solver(para, app)


    W0 = app.initial_cond(11)

    W = solver.solve(W0)

    V = app.to_pri(W)

    solver.plot(V[:,0], 'density', 0)

    solver.plot(V[:,1], 'velocity', 1)

    solver.plot(V[:,2], 'pressure', 2)

    solver.plot(V[:,1]/np.sqrt(gam * V[:, 2] / V[:, 0]), 'Mach', 3)
