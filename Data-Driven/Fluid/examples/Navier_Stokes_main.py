import sys, os
import matplotlib.pyplot as plt
import numpy as np

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("__file__"))) + '/apps')
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("__file__"))) + '/src')

from Solver import *
from Navier_Stokes import *


if __name__ == '__main__':
    para = {}
    para['temporal_scheme'] = 'RK2' # ForwardEuler or RK2
    para['inv_spatial_scheme'] = 'Linear' # Linear or Constant

    para['application'] = 'Navier_Stokes'
    mu = 0.012707143364911851


    para['L'] = L = 2. # domain size

    para['num_elem'] = n = 400 # number of elements

    dx = L/n

    para['time'] = 0.1 # simulate time

    para['cfl'] = 0.4 # cfl number
    para['boundary_condition'] = ['flow', 'flow'] # periodic, wall, or inflow
    para['output_dt'] = 0.4 # fixed time to output

    para['output_file'] = 'bin/Navier_Stokes'



    gam, R, Pr = 1.33, 1., 0.72
    eos = [gam, R, Pr]
    par = [True, 0, mu, eos]  # viscous or not; source term type; viscosity
    box = Box(n, dx)
    app = Navier_Stokes(par, box)
    solver = Solver(para, app)

    n = para['num_elem']

    W0 = app.initial_cond(2)

    W = solver.solve(W0)

    V = conser_to_pri(W, gam)

    solver.plot(V[:,0], 'density', 0)

    solver.plot(V[:,1], 'velocity', 1)

    solver.plot(V[:,2], 'pressure', 2)

    solver.plot(V[:,1]/np.sqrt(gam*V[:, 2]/V[:,0]), 'Mach', 3)
