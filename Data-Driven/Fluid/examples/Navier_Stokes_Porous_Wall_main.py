import sys, os
import matplotlib.pyplot as plt
import numpy as np

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("__file__"))) + '/apps')
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("__file__"))) + '/src')

from Solver import *
from Navier_Stokes_Porous_Wall import *

def run_mars(sample, init_cond, num_ele = 400, plot_or_not = False):
    para = {}
    para['temporal_scheme'] = 'RK2' # ForwardEuler or RK2
    para['inv_spatial_scheme'] = 'Linear' # Linear or Constant

    para['application'] = 'Navier_Stokes'
    mu = 0.012707143364911851


    para['L'] = L = 20. # domain size

    para['num_elem'] = n = num_ele # number of elements

    dx = L/n

    para['time'] = 10.0 # simulate time

    para['cfl'] = 0.4 # cfl number
    para['boundary_condition'] = ['flow', 'flow'] # periodic, wall, or inflow
    para['output_dt'] = 0.4 # fixed time to output

    para['output_file'] = 'bin/Navier_Stokes_' + sample + '_init%d_'%(init_cond)



    gam, R, Pr = 1.33, 1., 0.72
    eos = [gam, R, Pr]
    box = Box(n, dx)

    if(sample == 'square_2d'):
        par = [True, 1, mu, eos]  # viscous or not; source term type; viscosity
        porosity = 0.08
    elif(sample == 'square_3d'):
        par = [True, 3, mu, eos]  # viscous or not; source term type; viscosity
        porosity = 2./7. * 2./7.

    app = Navier_Stokes_Porous_Wall(par, box, porosity)
    solver = Solver(para, app)

    n = para['num_elem']

    W0 = app.initial_cond(init_cond)

    W = solver.solve(W0)

    V = conser_to_pri(W, gam)

    if(plot_or_not):
        solver.plot(V[:,0], 'density', 0)

        solver.plot(V[:,1], 'velocity', 1)

        solver.plot(V[:,2], 'pressure', 2)

        solver.plot(V[:,1]/np.sqrt(gam*V[:, 2]/V[:,0]), 'Mach', 3)

        solver.plot(V[:, 1] * V[:, 0], 'rho u', 4)


def run_nasa(sample, init_cond, num_ele = 400, plot_or_not = True):
    para = {}
    para['temporal_scheme'] = 'RK2' # ForwardEuler or RK2
    para['inv_spatial_scheme'] = 'Linear' # Linear or Constant

    para['application'] = 'Navier_Stokes'
    mu = 0.0035054277948174126


    para['L'] = L = 8. # domain size

    para['num_elem'] = n = num_ele # number of elements

    dx = L/n

    para['time'] = 0.5 # simulate time

    para['cfl'] = 0.4 # cfl number
    para['boundary_condition'] = ['flow', 'flow'] # periodic, wall, or inflow
    para['output_dt'] = 0.05 # fixed time to output

    para['output_file'] = 'XXbin/Navier_Stokes_' + sample + '_init%d_'%(init_cond)



    gam, R, Pr = 1.4, 1., 0.72
    eos = [gam, R, Pr]
    box = Box(n, dx)

    if(sample == 'square_2d'):
        par = [True, 1, mu, eos]  # viscous or not; source term type; viscosity
        porosity = 0.08
    elif(sample == 'square_3d'):
        par = [True, 3, mu, eos]  # viscous or not; source term type; viscosity
        porosity = 2./7. * 2./7.

    app = Navier_Stokes_Porous_Wall(par, box, porosity)
    solver = Solver(para, app)

    n = para['num_elem']

    W0 = app.initial_cond(init_cond)

    W = solver.solve(W0)

    V = conser_to_pri(W, gam)

    if (plot_or_not):
        solver.plot(V[:, 0], 'density', 0)

        solver.plot(V[:, 1], 'velocity', 1)

        solver.plot(V[:, 2], 'pressure', 2)

        solver.plot(V[:, 1] / np.sqrt(gam * V[:, 2] / V[:, 0]), 'Mach', 3)

        solver.plot(V[:, 1] * V[:, 0], 'rho u', 4)





if __name__ == '__main__':
    #run_mars('square_2d', 1, 400, True)
    #run_nasa('square_3d', 11, 400, True)
    for sample in ['square_3d', 'square_2d']:
        for init_cond in [1,2,3]:
            for n in [400]:
                run_mars(sample, init_cond, n)
