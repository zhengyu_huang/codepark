import sys, os
import matplotlib.pyplot as plt
import numpy as np

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("__file__"))) + '/apps')
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("__file__"))) + '/src')

from Solver import *
from Navier_Stokes_Nozzle import *

nozzle_cen = 10.
nozzle_w = 2.0
porosity = 0.08
def compute_A(xx):
    T = porosity
    A = 1 - (1-T)*np.cos(np.pi*(xx - nozzle_cen)/nozzle_w)**2
    A[xx < nozzle_cen - nozzle_w/2.0] = 1.
    A[xx > nozzle_cen + nozzle_w/2.0] = 1.
    return A

def compute_dA(xx):
    T = porosity
    dA = (1 - T) * 2*np.cos(np.pi * (xx - nozzle_cen) / nozzle_w) * np.sin(np.pi * (xx - nozzle_cen) / nozzle_w)*(np.pi/ nozzle_w)
    dA[xx < nozzle_cen - nozzle_w/2.0] = 0.
    dA[xx > nozzle_cen + nozzle_w/2.0] = 0
    return dA


if __name__ == '__main__':
    para = {}
    para['temporal_scheme'] = 'RK2' # ForwardEuler or RK2
    para['inv_spatial_scheme'] = 'Linear' # Linear or Constant

    para['application'] = 'Navier_Stokes_Nozzle'
    mu = 0.012707143364911851

    gam, R, Pr = 1.4, 1., 0.72
    eos = [gam, R, Pr]




    para['L'] = L = 20. # domain size
    para['num_elem'] = n = 400# number of elements

    dx = L/n

    xx_face = np.linspace(0, L, n + 1)
    xx = (xx_face[1:] + xx_face[0:n])/2
    A = compute_A(xx)
    A_face = compute_A(xx_face)
    dA = compute_dA(xx)


    para['time'] = 4.0 # simulate time

    para['cfl'] = 0.4 # cfl number

    gam, R, Pr = 1.4, 1., 0.72



    para['boundary_condition'] = ['flow', 'flow']
    para['output_dt'] = 0.4 # fixed time to output

    para['output_file'] = 'bin/Navier_Stokes_Nozzle'

    gam, R, Pr = 1.33, 1., 0.72

    eos = [gam, R, Pr]

    par = [True, 1, mu, eos]  # viscous or not; source term type; viscosity
    porosity = 0.08

    box = Box(n, dx)

    app = Navier_Stokes_Nozzle(par, box, porosity, A, A_face, dA)


    solver = Solver(para, app)


    W0 = app.initial_cond(1)

    W = solver.solve(W0)

    V = app.to_pri(W)

    solver.plot(V[:,0], 'density', 0)

    solver.plot(V[:,1], 'velocity', 1)

    solver.plot(V[:,2], 'pressure', 2)

    solver.plot(V[:,1]/np.sqrt(gam * V[:, 2] / V[:, 0]), 'Mach', 3)
