import sys, os
import matplotlib.pyplot as plt
import numpy as np
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("__file__"))) + '/apps')
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath("__file__"))) + '/src')
from Solver import *
from Burgers import Burgers
import Utility

def test_initial_cond(n):
    V0 = Burgers.initial_cond(1, n)
    V0_hat = Utility.spectrum(V0, 1)
    kk = np.arange(n)
    plt.loglog(kk[:n // 2], V0_hat[:n // 2])
    k_0 = 10
    A = 2. / (3. * np.sqrt(np.pi))
    plt.figure(1)
    plt.loglog(kk, A * kk ** 4 / k_0 ** 5 * np.exp(-kk ** 2 / k_0 ** 2), '--')
    plt.ylim([1e-15, 1e-1])
    plt.show()

def ensemble_spectrum(elem_n, file_pre, frame_n, sample_n, fig_n):
    #elem_n = 2**15
    V_hat_ave = np.zeros(elem_n)
    #sample_n = 128
    #frame_n = 5

    for sample_id in range(1,sample_n+1):
        file = file_pre  + '_sample%06d'%sample_id + '_ele%06d'%elem_n + '_frame%04d'%frame_n
        V = np.loadtxt(file, skiprows = 1, dtype = float)
        V_hat = Utility.spectrum(V, 1)
        V_hat_ave += V_hat
    V_hat_ave /= sample_n
    kappa = np.arange(elem_n)
    ref_hat = 1./kappa**2

    plt.figure(fig_n)
    plt.loglog(kappa[:elem_n], V_hat_ave[:elem_n], label = 'ele = %d'%elem_n)
    plt.loglog(kappa[1:elem_n//2],  ref_hat[1:elem_n//2], '--')
    plt.ylim([1e-20, 1e-1])
    plt.xlabel('kappa')
    plt.ylabel('E(kappa)')
    plt.legend()
    


def run(sample_id):
    para = {}
    para['temporal_scheme'] = 'RK2' # ForwardEuler or RK2
    para['inv_spatial_scheme'] = 'Linear' # Linear or Constant

    para['application'] = 'Burgers'
    mu = 5.e-4

    para['L'] = L = 2*np.pi # domain size
    para['num_elem'] = n =2**10# number of elements

    dx = L/n
    para['time'] = 0.5 # simulate time

    para['cfl'] = 1.0 # cfl number
    para['boundary_condition'] = ['periodic', 'periodic'] # periodic, wall, or inflow
    para['output_dt'] = 0.05 # fixed time to output


    para['output_file'] = 'bin/Burgers' + '_sample%06d'%sample_id

    box = Box(n,dx)
    par = [True, 0, mu]  # viscous or not; source term type; viscosity
    app = Burgers(par, box)
    solver = Solver(para, app)

    n = para['num_elem']

    np.random.seed(sample_id)

    V0 = Burgers.initial_cond(1, n)

    V = solver.solve(V0)


if __name__ == '__main__':
    print('Burgers Equation')
    #sample_id = int(sys.argv[1])
    sample_id = 1
    run(sample_id)
    
    #ensemble_spectrum(2**15, 'bin_2^15/Burgers', 5, 128, 0)
    #ensemble_spectrum(2**10, 'bin_2^10/Burgers', 5, 128, 0)
    #plt.show()
