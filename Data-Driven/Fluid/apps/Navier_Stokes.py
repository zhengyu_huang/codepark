# Subgrid modelling studies with Burgers' equation.  M. D. LOVE
# This file implement Burgers' equation
# Compute RHS
import numpy as np
from scipy import special
def conser_to_pri(W, gamma):
    V = np.empty(W.shape)
    if(W.ndim == 1):
        V[0] = W[0]
        V[1] = W[1] / W[0]
        V[2] = (W[2] - 0.5 * W[1] * V[1]) * (gamma - 1)
    else:
        V[:,0] = W[:,0]
        V[:,1] = W[:,1]/W[:,0]
        V[:,2] = (W[:,2] - 0.5*W[:,1]*V[:,1])*(gamma - 1)

    return V

def pri_to_conser(V, gamma):
    W = np.empty(V.shape)

    if (V.ndim == 1):
        W[0] = V[0]
        W[1] = V[0] * V[1]
        W[2] = 0.5 * V[0] * V[1] * V[1] + V[2] / (gamma - 1)
    else:
        W[:,0] = V[:,0]
        W[:,1] = V[:,0]*V[:,1]
        W[:,2] = 0.5*V[:,0]*V[:,1]*V[:,1] + V[:,2]/(gamma - 1)

    return W

def pri_to_energy(V, gamma):
    E = np.empty(V.shape[0])

    if (V.ndim == 1):
        E = 0.5 * V[0] * V[1] * V[1] + V[2] / (gamma - 1)
    else:
        E = 0.5*V[:,0]*V[:,1]*V[:,1] + V[:,2]/(gamma - 1)

    return E

# this is Fluid Fluid Roe Flux function
def flux_Roe(u_l, u_r, gamma):
    [rho_l, v_l, p_l] = u_l
    [rho_r, v_r, p_r] = u_r

    assert (p_r > 0 and rho_r > 0 and rho_l > 0 and p_l > 0)
    h_l = v_l * v_l / 2.0 + gamma * p_l / (rho_l * (gamma - 1))
    h_r = v_r * v_r / 2.0 + gamma * p_r / (rho_r * (gamma - 1))

    c_l = np.sqrt(gamma*p_l/rho_l)
    c_r = np.sqrt(gamma*p_r/rho_r)


    # compute the Roe-averaged quatities
    v_rl = (np.sqrt(rho_l) * v_l + np.sqrt(rho_r) * v_r) / (np.sqrt(rho_r) + np.sqrt(rho_l))
    h_rl = (np.sqrt(rho_l) * h_l + np.sqrt(rho_r) * h_r) / (np.sqrt(rho_r) + np.sqrt(rho_l))
    c_rl = np.sqrt((gamma - 1) * (h_rl - v_rl * v_rl / 2))
    rho_rl = np.sqrt(rho_r * rho_l)

    du = v_r - v_l
    dp = p_r - p_l
    drho = rho_r - rho_l
    du1 = du + dp / (rho_rl * c_rl)
    du2 = drho - dp / (c_rl * c_rl)
    du3 = du - dp / (rho_rl * c_rl)

    # compute the Roe-average wave speeds
    lambda_1 = v_rl + c_rl
    lambda_2 = v_rl
    lambda_3 = v_rl - c_rl

    # entropy correction
    lambda_l_1 = v_l + c_l
    lambda_l_2 = v_l
    lambda_l_3 = v_l - c_l

    lambda_r_1 = v_r + c_r
    lambda_r_2 = v_r
    lambda_r_3 = v_r - c_r

    eps_1 = max(0, lambda_1 - lambda_l_1, lambda_r_1 - lambda_1)
    eps_2 = max(0, lambda_2 - lambda_l_2, lambda_r_2 - lambda_2)
    eps_3 = max(0, lambda_3 - lambda_l_3, lambda_r_3 - lambda_3)

    if np.fabs(lambda_1) < eps_1:
        lambda_1 = 0.5*(lambda_1*lambda_1/eps_1 + eps_1)
    if np.fabs(lambda_2) < eps_2:
        lambda_2 = 0.5*(lambda_2*lambda_2/eps_2 + eps_2)
    if np.fabs(lambda_3) < eps_3:
        lambda_3 = 0.5*(lambda_3*lambda_3/eps_3 + eps_3)


    # compute the right characteristic vectors
    r_1 = rho_rl / (2 * c_rl) * np.array([1, v_rl + c_rl, h_rl + c_rl * v_rl])
    r_2 = np.array([1, v_rl, v_rl * v_rl / 2])
    r_3 = -rho_rl / (2 * c_rl) * np.array([1, v_rl - c_rl, h_rl - c_rl * v_rl])

    f_l = np.array([rho_l * v_l, rho_l * v_l * v_l + p_l, (rho_l * v_l * v_l / 2.0 + gamma * p_l / (gamma - 1)) * v_l])

    f_r = np.array([rho_r * v_r, rho_r * v_r * v_r + p_r, (rho_r * v_r * v_r / 2.0 + gamma * p_r / (gamma - 1)) * v_r])

    return 0.5 * (f_l + f_r) - 0.5 * (
    np.fabs(lambda_1) * du1 * r_1 + np.fabs(lambda_2) * du2 * r_2 + np.fabs(lambda_3) * du3 * r_3)


# this is Fluid Fluid Roe Flux function
def flux_Riemann(u_l, u_r, gamma):


    def _pressure_function(p, uu, gamma):
        '''
        Helper function, computing f_l(p,W_k), df_l(p,W_k)
        :param p: pressure
        :param uu: primitive state variables
        :param eos: equation of state
        :return:
        '''
        [rho_k, v_k, p_k] = uu
        if (p > p_k):  # shock

            A_k, B_k = 2 / ((gamma + 1) * rho_k), (gamma - 1) / (gamma + 1) * p_k

            f = (p - p_k) * np.sqrt(A_k / (p + B_k))

            df = np.sqrt(A_k / (B_k + p)) * (1 - (p - p_k) / (2 * (B_k + p)))

        else:  # rarefaction
            a_k = np.sqrt(gamma * p_k / rho_k)

            f = 2 * a_k / (gamma - 1) * ((p / p_k) ** ((gamma - 1) / (2 * gamma)) - 1)

            df = 1 / (rho_k * a_k) * (p / p_k) ** (- (gamma + 1) / (2 * gamma))

        return f, df

    def _pressure_function_init(uu_l, uu_r, gamma):
        '''
        guess the initial value of pressure at contact discontinuity
        :param uu_l: left state primitive variables
        :param uu_r: right state primitive variables
        :param gamma: equation of state

        '''
        [rho_l, v_l, p_l] = uu_l

        [rho_r, v_r, p_r] = uu_r

        return 0.5 * (p_l + p_r)

    def _solve_contact_discontinuity(uu_l, uu_r, gamma):
        '''
        solve function f(p,W_l,W_r) = f_l(p,W_l) + f_r(p,w_r) + delta v
        :param p: pressure guess at contact discontinuity
        :param uu_l: left state primitive variables
        :param uu_r: right state primitive variables
        :param eos: equation of state
        :return: p,v around the contact discontinuity
        '''

        [rho_l, v_l, p_l] = uu_l
        [rho_r, v_r, p_r] = uu_r

        d_v = v_r - v_l

        MAX_ITE = 100;
        TOLERANCE = 1.0e-12
        found = False

        p_old = _pressure_function_init(uu_l, uu_r, gamma)

        for i in range(MAX_ITE):
            f_l, df_l = _pressure_function(p_old, uu_l, gamma)
            f_r, df_r = _pressure_function(p_old, uu_r, gamma)

            p = p_old - (f_l + f_r + d_v) / (df_l + df_r)

            if (p < 0.0):
                p = TOLERANCE

            if (2 * abs(p - p_old) / (p + p_old) < TOLERANCE):
                found = True
                break
            p_old = p

        if not found:
            print('Divergence in Newton-Raphason iteration')

        v = 0.5 * (v_l + v_r + f_r - f_l)

        return p, v


    def _sample_solution(p_m, v_m, uu_l, uu_r, gamma, s):
        '''
        :param p: pressure at contact discontinuity
        :param v: velocity at contact discontinuity
        :param uu_l: left primitive variables
        :param uu_r: right primitive variables
        :param gamma: equation of state
        :param s: sample point satisfies s = x/t
        :return: rho, v , p at (t,x)
        '''

        [rho_l, v_l, p_l] = uu_l
        a_l = np.sqrt(gamma * p_l / rho_l)

        [rho_r, v_r, p_r] = uu_r
        a_r = np.sqrt(gamma * p_r / rho_r)

        if s < v_m:
            # sampling point lies to the left of the contact discontinuity
            if p_m < p_l:

                # left rarefaction
                s_l = v_l - a_l
                a_ml = a_l * (p_m / p_l) ** ((gamma - 1) / (2 * gamma))
                s_ml = v_m - a_ml
                if s < s_l:  # left state
                    rho, v, p = rho_l, v_l, p_l

                elif s < s_ml:  # left rarefaction wave
                    rho = rho_l * (2 / (gamma + 1) + (gamma - 1) / ((gamma + 1) * a_l) * (v_l - s)) ** (2 / (gamma - 1))
                    v = 2 / (gamma + 1) * (a_l + (gamma - 1) / 2.0 * v_l + s)
                    p = p_l * (2 / (gamma + 1) + (gamma - 1) / ((gamma + 1) * a_l) * (v_l - s)) ** (
                    2 * gamma / (gamma - 1))

                else:  # left contact discontinuity
                    rho, v, p = rho_l * (p_m / p_l) ** (1 / gamma), v_m, p_m
            else:
                # left shock

                s_shock = v_l - a_l * np.sqrt((gamma + 1) * p_m / (2 * gamma * p_l) + (gamma - 1) / (2 * gamma))

                if s < s_shock:
                    rho, v, p = rho_l, v_l, p_l
                else:
                    rho = rho_l * (p_m / p_l + (gamma - 1) / (gamma + 1)) / (
                    (gamma - 1) * p_m / ((gamma + 1) * p_l) + 1)
                    v = v_m
                    p = p_m
        else:
            # sampling point lies to the right of the contact discontinuity

            if p_m < p_r:

                # right rarefaction
                s_r = v_r + a_r
                a_mr = a_r * (p_m / p_r) ** ((gamma - 1) / (2 * gamma))
                s_mr = v_m + a_mr
                if s > s_r:  # left state
                    rho, v, p = rho_r, v_r, p_r

                elif s > s_mr:  # left rarefaction wave
                    rho = rho_r * (2 / (gamma + 1) - (gamma - 1) / ((gamma + 1) * a_r) * (v_r - s)) ** (2 / (gamma - 1))
                    v = 2 / (gamma + 1) * (-a_r + (gamma - 1) / 2.0 * v_r + s)
                    p = p_r * (2 / (gamma + 1) - (gamma - 1) / ((gamma + 1) * a_r) * (v_r - s)) ** (
                    2 * gamma / (gamma - 1))

                else:  # left contact discontinuity
                    rho, v, p = rho_r * (p_m / p_r) ** (1 / gamma), v_m, p_m
            else:
                # right shock

                s_shock = v_r + a_r * np.sqrt((gamma + 1) * p_m / (2 * gamma * p_r) + (gamma - 1) / (2 * gamma))

                if s > s_shock:  # after shock
                    rho, v, p = rho_r, v_r, p_r
                else:
                    # preshock, contact discontinuity
                    rho = rho_r * (p_m / p_r + (gamma - 1) / (gamma + 1)) / (
                    (gamma - 1) * p_m / ((gamma + 1) * p_r) + 1)
                    v = v_m
                    p = p_m
        return rho, v, p

    p_m, v_m = _solve_contact_discontinuity(u_l, u_r, gamma)

    rho, v, p = _sample_solution(p_m, v_m, u_l, u_r, gamma, 0.0)

    return np.array([rho*v, rho*v*v + p, v*(0.5*rho*v*v + p*gamma/(gamma - 1))])



class Navier_Stokes:
    def __init__(self, paras, box):
        viscous, source_term_type, mu, eos = paras

        gam, R, Pr = eos
        c_v, c_p = R/(gam - 1), R*gam/(gam - 1)

        self._mu, self._lam, self._kappa = mu, -2./3.*mu, gam*mu*c_v/Pr
        self._eos = eos
        self._box = box
        self._vis = viscous
        self._source = source_term_type
        self._flux_func = flux_Riemann

    def name(self):
        return 'Navier_Stokes'

    def num_eq(self):
        return 3

    def initial_cond(self, type):
        n = self._box.get_size()
        dx = self._box.get_dx()
        L = dx*n
        gam = self._eos[0]

        if type == 0:
            #sod problem
            V = np.empty([n,3])
            n_L = n//2
            V[:n_L, 0], V[n_L:, 0] = 1. , 0.125
            V[:n_L, 1], V[n_L:, 1] = 0. , 0.
            V[:n_L, 2], V[n_L:, 2] = 1. , 0.1


        if type == 1:
            # Zahr, Matthew J., and P-O. Persson.
            # "An optimization-based approach for high-order accurate discretization of conservation
            #  laws with discontinuous solutions."
            # Journal of Computational Physics (2018).
            V = np.empty([n,3])
            n_L = n//2
            V[:n_L, 0], V[n_L:, 0] = 1. , 1.0
            V[:n_L, 1], V[n_L:, 1] = 1. , 1.0
            V[:n_L, 2], V[n_L:, 2] = 1./(0.4*0.4*gam) , 1/(0.45*0.45*gam)

        if type == 2:
            #Mars condition
            V = np.empty([n,3])
            xx = (np.arange(n) + 0.5)*dx
            x_interface = 9.5
            C_i = 1.e-15
            f_sm = 0.5*(1. + special.erf((xx - x_interface)/(C_i*dx)))

            # rho_l, u_l, p_l = 1.0,                1.0, 0.7518796992481204
            # rho_r, u_r, p_r = 0.3649922771465224, 0.0, 0.1770957021238189

            rho_l, u_l, p_l = 1.0, 0.6, 0.7518796992481204
            rho_r, u_r, p_r = 0.5419203691155778, 0.0, 0.32599013828916323

            # rho_l, u_l, p_l = 1.0, 1.2, 0.7518796992481204
            # rho_r, u_r, p_r = 0.30276434298491006, 0.0, 0.12658085012624493


            V[:, 0] = rho_l*(1 - f_sm) + rho_r*f_sm
            V[:, 1] = u_l*(1 - f_sm) + u_r*f_sm
            V[:, 2] = p_l*(1 - f_sm) + p_r*f_sm

            # V[:, 0] = 1.3764*(1 - f_sm) + 1.*f_sm
            # V[:, 1] = 0.3336*(1 - f_sm) + 0.*f_sm
            # V[:, 2] = 1.5698/1.4*(1 - f_sm) + 1./1.4*f_sm

        return pri_to_conser(V, gam)

    def is_viscous(self):
        return self._vis

    def has_source(self):
        return (self._source > 0)

    def invisid_term(self, V_minus, V_plus, t):
        '''
        :param self:
        :param V_minus: primitive state variables on the left
        :param V_plus: primitive state variables on the right
        :return: upwind flux
        '''

        n = self._box.get_size()
        dx = self._box.get_dx()
        gam = self._eos[0]

        assert(n+1 == len(V_minus))

        f = np.empty([n+1, 3])


        for i in range(n+1):
            f[i,:] = self._flux_func(V_minus[i,:], V_plus[i,:], gam)

        return (f[0:n,:] - f[1:n+1,:])/dx

    def viscous_term(self, V, t):
        '''
        :param self:
        :param V: primitive variable vector of size n+2, with ghost values on both sides
        :return:
        '''

        n = self._box.get_size()

        dx = self._box.get_dx()

        mu, lam, kappa = self._mu, self._lam, self._kappa

        gam, R = self._eos[0], self._eos[1]

        vel = V[:,1]

        T = V[:,2]/(R*V[:, 0])

        ddvel = (vel[2:n+2] - 2*vel[1:n+1] + vel[0:n])/(dx*dx)
        dvel = (vel[2:n+2] - vel[0:n])/(2*dx)
        ddT = (T[2:n+2] - 2*T[1:n+1] + T[0:n])/(dx*dx)

        f = np.empty([n, 3])
        f[:,0] = 0.0
        f[:,1] = (2*mu + lam)*ddvel
        f[:,2] = (2*mu + lam)*(ddvel*vel[1:n+1] + dvel*dvel) + kappa*ddT

        return f



    def source_term(self, V, t):
        n = self._box.get_size()
        eqn = self.num_eq()
        return np.zeros([n,eqn])

    def max_dt(self, W, cfl = 0.8):
        # For inviscid dt <= dx / (|u| + c)_max
        # For viscous  dt <= 1/( (|u| + c)_max/dx + 2mu/dx^2)
        dx = self._box.get_dx()

        gam = self._eos[0]

        V = self.to_pri(W)

        max_wave_speed = np.fabs(V[:,1]) + np.sqrt(gam*V[:,2]/V[:,0])

        stable_spectral_radius = np.max(max_wave_speed)/dx

        if self.is_viscous():
            stable_spectral_radius += 2*max(self._mu, self._kappa)/(dx*dx)

        return cfl/(stable_spectral_radius + 1.e-15)

    def mirror_state(self, w):
        w_mir = np.copy(w)
        w_mir[1] = -w_mir[1]
        return w_mir

    def need_variable_change(self):
        return True

    def to_pri(self, W):
        gam = self._eos[0]
        return conser_to_pri(W, gam)