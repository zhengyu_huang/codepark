import numpy as np
from scipy import special
from Navier_Stokes import *

def flux_half_Riemann(u_l, v, fluid_normal, gam):
    '''
    :param u_l: fluid state primitive variable
    :param v:  wall speed
    :param fluid_normal: normal of the interface
           if fluid_normal = 1 wall is on the right
           if fluid_normal =-1 wall is on the left
    :param gam:
    :return: fluid primitive state at the wall
    '''
    [rho_l, v_l, p_l] = u_l;
    #left facing shock case
    if(v_l*fluid_normal > v*fluid_normal):
        a = 2/((gam + 1)*rho_l);
        b = p_l*(gam - 1)/(gam + 1)
        phi = a/(v - v_l)**2

        p = p_l + (1 + np.sqrt(4*phi*(p_l + b) + 1))/(2*phi)
        rho = rho_l*(p/p_l + (gam - 1)/(gam + 1))/(p/p_l * (gam - 1)/(gam + 1) + 1)
    #left facing rarefactions case
    else:
        c_l = np.sqrt(gam*p_l/rho_l)
        p = p_l*(-(gam - 1)/(2*c_l)*(v - v_l)*fluid_normal + 1)**(2*gam/(gam - 1))
        rho = rho_l*(p/p_l)**(1/gam)
    u_s = np.array([rho, v, p])


    #todo test ghost mirroring
    #u_s = [rho_l, 2*v - v_l, p_l]
    return u_s


class Navier_Stokes_Porous_Wall(Navier_Stokes):
    def __init__(self, paras, box, porosity):
        '''
        there is a porous wall between cell n//2 - 1 and n//2
        :param paras:
        :param box:
        :param porosity: void fraction of the wall
        '''
        Navier_Stokes.__init__(self, paras, box)
        self._porosity = porosity


    def name(self):
        return Navier_Stokes.name(self) + '_Porous_Wall'



    def invisid_term(self, V_minus, V_plus, t):
        '''
        todo need V or not
        :param self:
        :param V_minus: primitive state variables on the left
        :param V_plus: primitive state variables on the right
        :return: upwind flux
        '''

        n = self._box.get_size()
        dx = self._box.get_dx()
        gam = self._eos[0]
        alpha = self._porosity

        assert(n+1 == len(V_minus))

        f = np.empty([n+1, 3])


        for i in range(n+1):
            f[i,:] = self._flux_func(V_minus[i,:], V_plus[i,:], gam)


        # modify the flux through the porous wall, between cell n//2-1, n//2

        u_l, u_r = V_minus[n // 2], V_plus[n // 2]

        #############################################

        # u_l_p = np.array([u_l[0], u_l[1] / alpha, u_l[2]])
        # u_r_p = np.array([u_r[0], u_r[1] / alpha, u_r[2]])
        #
        # f[n//2,:] = self._flux_func(u_l_p, u_r_p, gam)


        #######################################################

        n_L, n_R = n//2 - 1, n//2

        vs = 0.0 #porous wall velocity

        # compute the left flux

        u_rR = flux_half_Riemann(u_l, vs, 1.0, gam)


        f_l = (1 - alpha) * self._flux_func(u_l, u_rR, gam) + alpha * f[n // 2, :]

        # compute the right flux

        u_lR = flux_half_Riemann(u_r, vs, -1.0, gam)

        f_r = (1 - alpha) * self._flux_func(u_lR, u_r, gam) + alpha * f[n // 2, :]


        R = (f[0:n,:] - f[1:n+1,:])/dx

        R[n_L, :] = (f[n//2 - 1, :] - f_l)/dx

        R[n_R, :] = (f_r - f[n//2 + 1, :])/dx

        return R

    def viscous_term(self, V, t):
        '''
        :param self:
        :param V: primitive variable vector of size n+2, with ghost values on both sides
        :return:
        '''
        alpha = self._porosity

        n = self._box.get_size()

        dx = self._box.get_dx()

        mu, lam, kappa = self._mu, self._lam, self._kappa

        gam, R = self._eos[0], self._eos[1]

        vel = V[:,1]

        T = V[:,2]/(R*V[:, 0])

        ddvel = (vel[2:n+2] - 2*vel[1:n+1] + vel[0:n])/(dx*dx)
        dvel = (vel[2:n+2] - vel[0:n])/(2*dx)
        ddT = (T[2:n+2] - 2*T[1:n+1] + T[0:n])/(dx*dx)


        #need to modify the viscous term ddvel, dvel, and ddT at index = n//2 and n//2 + 1

        n_L, n_R = n // 2 - 1, n // 2

        v_gr = alpha * vel[n//2 + 1] + (1. - alpha)* (-vel[n//2])
        v_gl = alpha * vel[n//2] + (1. - alpha)* (-vel[n//2 + 1])

        ddvel[n_L] = (vel[n//2 - 1] - 2*vel[n//2] + v_gr)/(dx*dx)
        ddvel[n_R] = (v_gl - 2 * vel[n // 2 + 1] + vel[n // 2 + 2]) / (dx * dx)

        dvel[n_L] = (v_gr - vel[n // 2 - 1]) / (2 * dx)
        dvel[n_R] = (vel[n // 2 + 2] - v_gl) / (2 * dx)

        T_gr = alpha * T[n//2 + 1] + (1. - alpha)* T[n//2]
        T_gl = alpha * T[n//2] + (1. - alpha)* T[n//2 + 1]

        ddT[n_L] = (T[n // 2 - 1] - 2 * T[n // 2] + T_gr) / (dx * dx)
        ddT[n_R] = (T_gl - 2 * T[n // 2 + 1] + T[n // 2 + 2]) / (dx * dx)



        f = np.empty([n, 3])
        f[:,0] = 0.0
        f[:,1] = (2*mu + lam)*ddvel
        f[:,2] = (2*mu + lam)*(ddvel*vel[1:n+1] + dvel*dvel) + kappa*ddT

        return f



    def source_term(self, V, t):
        n = self._box.get_size()
        eqn = self.num_eq()
        S = np.zeros([n,eqn])
        alpha = self._porosity
        mu = self._mu
        dx = self._box.get_dx()



        if self._source  == 1:
            #This is for 2D porous wall
            #there are momentum loss and energy loss


            xx = (np.arange(n) + 0.5) * dx
            # The size of the whole
            hole_y = alpha/2.0

            v = V[:, 1]



            x0 = n*dx/2.0
            # sigma = 0.08
            # f_sm = 1./(sigma*np.sqrt(2*np.pi)) * np.exp( -(xx - x0)*(xx - x0)/(2.*sigma*sigma))
            sigma = 0.16/np.sqrt(2*np.pi)
            f_sm = 4.*np.exp( -(xx - x0)*(xx - x0)/(2.*sigma*sigma))


            S[:, 1] = -mu*3*v/(hole_y*hole_y) * f_sm

        elif self._source == 2:
            # This is for 3D porous wall, circular cross-section
            # there are momentum loss and energy loss
            xx = (np.arange(n) + 0.5) * dx
            # The size of the whole
            hole_y = np.sqrt(alpha/np.pi)

            v = V[:, 1]

            x0 = n * dx / 2.0
            sigma = 0.08
            f_sm = 1. / (sigma * np.sqrt(2 * np.pi)) * np.exp(-(xx - x0) * (xx - x0) / (2. * sigma * sigma))

            S[:, 1] = -mu * 8 * v / (hole_y * hole_y) * f_sm

        elif self._source == 3:
            # This is for 3D porous wall, square cross-section
            # there are momentum loss and energy loss
            xx = (np.arange(n) + 0.5) * dx
            # The size of the whole
            hole_y = np.sqrt(alpha / 4)

            v = V[:, 1]

            x0 = n * dx / 2.0
            sigma = 0.16/np.sqrt(2*np.pi)
            #f_sm = 1. / (sigma * np.sqrt(2 * np.pi)) * np.exp(-(xx - x0) * (xx - x0) / (2. * sigma * sigma))
            f_sm = np.exp(-(xx - x0) * (xx - x0) / (2. * sigma * sigma))
            xi = 0.421731044865
            S[:, 1] = -mu*3*v/xi/(hole_y * hole_y) * f_sm

        return S


    def initial_cond(self, type):
        n = self._box.get_size()
        dx = self._box.get_dx()
        L = dx * n
        gam = self._eos[0]

        if type == 0:
            # sod problem
            V = np.empty([n, 3])
            n_L = n // 2
            V[:n_L, 0], V[n_L:, 0] = 1., 0.125
            V[:n_L, 1], V[n_L:, 1] = 0., 0.
            V[:n_L, 2], V[n_L:, 2] = 1., 0.1

        if type == 1 or type == 2 or type == 3:
            # Mars condition
            V = np.empty([n, 3])
            xx = (np.arange(n) + 0.5) * dx
            x_interface = 9.5
            C_i = 1.e-15
            f_sm = 0.5 * (1. + special.erf((xx - x_interface) / (C_i * dx)))


            if type == 1:
                rho_l, u_l, p_l = 1.0, 0.6, 0.7518796992481204
                rho_r, u_r, p_r = 0.5419203691155778, 0.0, 0.32599013828916323
            if type == 2:
                rho_l, u_l, p_l = 1.0,                1.0, 0.7518796992481204
                rho_r, u_r, p_r = 0.3649922771465224, 0.0, 0.1770957021238189
            if type == 3:
                rho_l, u_l, p_l = 1.0, 1.2, 0.7518796992481204
                rho_r, u_r, p_r = 0.30276434298491006, 0.0, 0.12658085012624493

            V[:, 0] = rho_l * (1 - f_sm) + rho_r * f_sm
            V[:, 1] = u_l * (1 - f_sm) + u_r * f_sm
            V[:, 2] = p_l * (1 - f_sm) + p_r * f_sm

            # V[:, 0] = 1.3764*(1 - f_sm) + 1.*f_sm
            # V[:, 1] = 0.3336*(1 - f_sm) + 0.*f_sm
            # V[:, 2] = 1.5698/1.4*(1 - f_sm) + 1./1.4*f_sm


        if type == 11 or type == 12 or type == 13 or type == 14 or type == 15 or type == 16 or type == 17:
            # Mars condition
            V = np.empty([n, 3])
            xx = (np.arange(n) + 0.5) * dx
            x_interface = L/2.0
            C_i = 1.e-15
            f_sm = 0.5 * (1. + special.erf((xx - x_interface) / (C_i * dx)))

            if type == 11:
                rho_l, u_l, p_l = 1.0, 0.0, 946.25
                rho_r, u_r, p_r = 0.9989431968295907, 0.0, 945.25
            if type == 12:
                rho_l, u_l, p_l = 1.0, 0.0, 946.25
                rho_r, u_r, p_r = 0.9978863936591811, 0.0, 944.25
            if type == 13:
                rho_l, u_l, p_l = 1.0, 0.0, 946.25
                rho_r, u_r, p_r = 0.9947159841479526, 0.0, 941.25
            if type == 14:
                rho_l, u_l, p_l = 1.0, 0.0, 946.25
                rho_r, u_r, p_r = 0.989431968295905, 0.0, 936.25
            if type == 15:
                rho_l, u_l, p_l = 1.0, 0.0, 946.25
                rho_r, u_r, p_r = 0.9788639365918099, 0.0, 926.25
            if type == 16:
                rho_l, u_l, p_l = 1.0, 0.0, 946.25
                rho_r, u_r, p_r = 0.9682959048877147, 0.0, 916.25
            if type == 17:
                rho_l, u_l, p_l = 1.0, 0.0, 946.25
                rho_r, u_r, p_r = 0.9577278731836196, 0.0, 906.25

            V[:, 0] = rho_l * (1 - f_sm) + rho_r * f_sm
            V[:, 1] = u_l * (1 - f_sm) + u_r * f_sm
            V[:, 2] = p_l * (1 - f_sm) + p_r * f_sm

            # V[:, 0] = 1.3764*(1 - f_sm) + 1.*f_sm
            # V[:, 1] = 0.3336*(1 - f_sm) + 0.*f_sm
            # V[:, 2] = 1.5698/1.4*(1 - f_sm) + 1./1.4*f_sm

        return pri_to_conser(V, gam)