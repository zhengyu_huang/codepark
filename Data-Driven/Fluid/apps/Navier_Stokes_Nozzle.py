# Benison, Gerald I., and Ephraim L. Rubin. "A time-dependent analysis for quasi-one-dimensional, viscous,
# heat conducting, compressible Laval nozzle flows."
# Journal of Engineering Mathematics 5, no. 1 (1971): 39-49.
#
# Glimm, James, Guillermo Marshall, and Bradley Plohr.
# "A generalized Riemann problem for quasi-one-dimensional gas flows."
# Advances in Applied Mathematics 5, no. 1 (1984): 1-30.
import numpy as np
from Navier_Stokes import *

class Navier_Stokes_Nozzle(Navier_Stokes):
    def __init__(self, paras, box, porosity, A, A_face, dA):
        '''
        :param paras:
        :param box:
        :param A: array of size n, nozzle area at xx[0], xx[1] ... xx[n-1]
        :param A_face: array of size n+1, nozzle area at faces xx[-0.5] xx[0.5], xx[1.5] ... xx[n-0.5]
        :param dA: array of size n, nozzle area derivative at xx[0], xx[1] ... xx[n-1]
        '''
        Navier_Stokes.__init__(self, paras, box)
        assert(self._source > 0)
        self._A = A
        self._A_face = A_face
        self._dA = dA
        self._porosity = porosity

    def name(self):
        return Navier_Stokes.name(self) + '_Nozzle'





    def viscous_term(self, V, t):
        '''
        :param self:
        :param V: primitive variable vector of size n+2, with ghost values on both sides
        :return:
        '''

        n = self._box.get_size()

        dx = self._box.get_dx()

        mu, lam, kappa = self._mu, self._lam, self._kappa

        gam, R = self._eos[0], self._eos[1]

        A, dA = self._A, self._dA

        vel = V[:,1]

        T = V[:,2]/(R*V[:, 0])

        ddvel = (vel[2:n+2] - 2*vel[1:n+1] + vel[0:n])/(dx*dx)
        dvel = (vel[2:n+2] - vel[0:n])/(2*dx)
        ddT = (T[2:n+2] - 2*T[1:n+1] + T[0:n])/(dx*dx)

        f = np.empty([n, 3])
        f[:,0] = 0.0
        f[:,1] = (2*mu + lam)*ddvel + (2*mu + lam)*dvel*dA/A
        f[:,2] = (2*mu + lam)*(ddvel*vel[1:n+1] + dvel*dvel + dvel*vel[1:n+1]*dA/A) + kappa*(ddT + ddT*dA/A)

        return f



    def source_term(self, V, t):
        gam = self._eos[0]
        E = pri_to_energy(V, gam)
        S = np.zeros(V.shape)

        n = self._box.get_size()

        alpha = self._porosity
        mu = self._mu
        dx = self._box.get_dx()



        A, dA = self._A, self._dA
        S[:, 0] = -V[:, 0] * V[:, 1]*dA/A
        S[:, 1] = -V[:, 0]*V[:, 1]*V[:, 1]*dA/A
        S[:, 2] = -(E + V[:, 2])*V[:, 1] * dA / A


        if self._source  == 1:
            #This is for 2D porous wall
            #there are momentum loss and energy loss


            xx = (np.arange(n) + 0.5) * dx
            # The size of the whole
            hole_y = alpha/2.0

            v = V[:, 1]



            x0 = n*dx/2.0
            # sigma = 0.08
            # f_sm = 1./(sigma*np.sqrt(2*np.pi)) * np.exp( -(xx - x0)*(xx - x0)/(2.*sigma*sigma))
            sigma = 0.16/np.sqrt(2*np.pi)
            f_sm = 4.*np.exp( -(xx - x0)*(xx - x0)/(2.*sigma*sigma))


            S[:, 1] += -mu*3*v/(hole_y*hole_y) * f_sm

        elif self._source == 2:
            # This is for 3D porous wall, circular cross-section
            # there are momentum loss and energy loss
            xx = (np.arange(n) + 0.5) * dx
            # The size of the whole
            hole_y = np.sqrt(alpha/np.pi)

            v = V[:, 1]

            x0 = n * dx / 2.0
            sigma = 0.08
            f_sm = 1. / (sigma * np.sqrt(2 * np.pi)) * np.exp(-(xx - x0) * (xx - x0) / (2. * sigma * sigma))

            S[:, 1] += -mu * 8 * v / (hole_y * hole_y) * f_sm

        elif self._source == 3:
            # This is for 3D porous wall, square cross-section
            # there are momentum loss and energy loss
            xx = (np.arange(n) + 0.5) * dx
            # The size of the whole
            hole_y = np.sqrt(alpha / 4)

            v = V[:, 1]

            x0 = n * dx / 2.0
            sigma = 0.16/np.sqrt(2*np.pi)
            #f_sm = 1. / (sigma * np.sqrt(2 * np.pi)) * np.exp(-(xx - x0) * (xx - x0) / (2. * sigma * sigma))
            f_sm = np.exp(-(xx - x0) * (xx - x0) / (2. * sigma * sigma))
            xi = 0.421731044865
            S[:, 1] += -mu*3*v/xi/(hole_y * hole_y) * f_sm

        return S



