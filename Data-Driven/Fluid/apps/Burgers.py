# Subgrid modelling studies with Burgers' equation.  M. D. LOVE
# This file implement Burgers' equation
# Compute RHS

import numpy as np
class Burgers:
    def __init__(self, paras, box):
        viscous, source_term_type, nu = paras
        self._nu = nu
        self._box = box
        self._vis = viscous
        self._source = source_term_type

    def name(self):
        return 'Burgers'

    def num_eq(self):
        return 1

    @staticmethod
    def initial_cond(type, n):
        if type == 0:
            #sin(2*pi*x/L)
            return np.sin(2*np.pi*(np.arange(n) - 0.5)/n)
        elif type == 1:
            print('initialize freely decaying Burgers turbulence')
            # domain size is [0, 2pi]
            def energy_spectrum(N):
                # base on the energy spectrum: E(k) = \frac{2}{3\sqrt{\pi}}\frac{k^4}{k_0^5}e^{-(k/k0)^2}
                # u = sum_{k = -N/2}^{ N/2 - 1}
                k = np.arange(-N//2, N//2, 1)
                k_0 = 10
                A = 2./(3.*np.sqrt(np.pi))
                E = A*k**4/k_0**5 * np.exp(-k**2/k_0**2)
                return E

            random_phase = np.random.uniform(0, 2*np.pi, n)
            random_phase[n // 2 + 1:] = -random_phase[n // 2 - 1: 0: -1]
            random_phase[0] = 0.0 if random_phase[0] < np.pi else np.pi

            u_hat = np.sqrt(energy_spectrum(n)) * np.exp(random_phase * 1.0j)

            v_hat = np.fft.ifftshift(u_hat)
            u = np.real(np.fft.ifft(v_hat))*n

            return u






    def is_viscous(self):
        return self._vis

    def has_source(self):
        return (self._source > 0)

    def invisid_term(self, V_minus, V_plus, t):
        '''
        :param self:
        :param V_minus: state variable on the left
        :param V_plus: state variable on the right
        :return: upwind flux
        '''

        n = self._box.get_size()
        dx = self._box.get_dx()

        assert(n+1 == len(V_minus))

        f = np.empty([n+1, 1])
        f_ind = (V_minus + V_plus) > 0
        f_minus, f_plus = 0.5*V_minus**2 , 0.5*V_plus**2

        f[f_ind], f[~f_ind] = f_minus[f_ind], f_plus[~f_ind]


        return (f[0:n,:] - f[1:n+1,:])/dx

    def viscous_term(self, V, t):
        '''
        :param self:
        :param V: a vector of size n+2, with ghost values on both sides
        :return:
        '''

        n = self._box.get_size()

        dx = self._box.get_dx()

        nu = self._nu

        f = nu/(dx*dx) * (V[2:n+2,:] - 2*V[1:n+1,:] + V[0:n,:])

        return f



    def source_term(self, V, t):
        n = self._box.get_size()
        return np.zeros([n,1])

    def max_dt(self, V, cfl = 0.8):
        # For inviscid dt <= dx / u_max
        # For viscous  dt <= 1/(u_max/dx + 2mu/dx^2)
        dx = self._box.get_dx()
        stable_spectral_radius = max(abs(V.min()), abs(V.max()))/dx
        if self.is_viscous():
            stable_spectral_radius += 2*self._nu/(dx*dx)

        return cfl/(stable_spectral_radius + 1.e-15)


    def need_variable_change(self):
        return False

    def mirror_state(self, v):
        return -v