import numpy as np
class Box:
    def __init__(self, n, dx):
        self._n = n
        self._dx = dx
    def get_dx(self):
        return self._dx
    def get_size(self):
        return self._n





def spectrum(uu, dim = 1):
    n = len(uu)
    u_hat = np.fft.fft(uu)/n
    return np.absolute(u_hat)**2