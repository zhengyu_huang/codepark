import numpy as np
import matplotlib.pyplot as plt
import Limiter
from Burgers import Burgers
from Navier_Stokes import Navier_Stokes
from Utility import Box
class Solver:
    def __init__(self, para, app):
        '''
        :param self:
        :return:
        '''

        if 'temporal_scheme' in para:
            self._temporal_scheme = para['temporal_scheme']

        if 'inv_spatial_scheme' in para:
            self._spatial_scheme = para['inv_spatial_scheme']
            if para['inv_spatial_scheme'] == 'Constant':
                self._n_inv_ghost = 1
                self._reconstruct_func = Limiter.constant
            elif para['inv_spatial_scheme'] == 'Linear':
                    self._n_inv_ghost = 2
                    self._reconstruct_func = Limiter.Van_Leer

        #todo always linear for viscous flux
        self._n_vis_ghost = 1

        if 'num_elem' in para:
            self._n = para['num_elem']

        if 'L' in para:
            self._L = para['L']
            self._dx = self._L/self._n

        if 'dt' in para:
            self._dt = para['dt']
        else:
            self._dt = -1

        if 'cfl' in para:
            self._cfl = para['cfl']

        if 'time' in para:
            self._T = para['time']

        if 'output_file' in para:
            self._output_file = para['output_file']
            self._frame = 0

        if 'output_dt' in para:
            self._output_dt = para['output_dt']



        #Set apps
        self._app = app
        self._n_eqn =  app.num_eq()



        if 'boundary_condition' in para:
            self._bc = para['boundary_condition']


    def _populate_ghost(self, V, n_ghost):
        '''

        :param V: primitive variables!!
        :param n_ghost:
        :return:
        '''
        app = self._app

        n = self._n
        bc = self._bc
        n_eqn = self._n_eqn



        V_ext = np.empty([n + 2*n_ghost, n_eqn])

        V_ext[n_ghost:n + n_ghost,:] = V

        #periodic
        if bc[0] == 'periodic':
            assert(bc[1] == 'periodic')

            for i in range(n_ghost):
                #0, .. ghost-1,| ghost .... ghost + n - 1,| ghost + n ... ,2*ghost+n -1
                V_ext[i, :] = V[n - n_ghost + i,:]
                V_ext[n + n_ghost + i, :] = V[i, :]

        #periodic
        elif bc[0] == 'wall' or bc[1] == 'wall':
            if bc[0] == 'wall':
                for i in range(n_ghost):
                    #0, .. ghost-1,| ghost .... ghost + n - 1,| ghost + n ... ,2*ghost+n -1
                    V_ext[i, :] = app.mirror_state(V[n_ghost - i - 1,:])

            if bc[1] == 'wall':
                for i in range(n_ghost):
                    #0, .. ghost-1,| ghost .... ghost + n - 1,| ghost + n ... ,2*ghost+n -1
                    V_ext[n + n_ghost + i, :] = app.mirror_state(V[n - i - 1,:])

        # flow, constant extrapolation from inside
        elif bc[0] == 'flow' or bc[1] == 'flow':
            if bc[0] == 'flow':
                for i in range(n_ghost):
                    # 0, .. ghost-1,| ghost .... ghost + n - 1,| ghost + n ... ,2*ghost+n -1
                    V_ext[i, :] = V[0, :]

            if bc[1] == 'flow':
                for i in range(n_ghost):
                    # 0, .. ghost-1,| ghost .... ghost + n - 1,| ghost + n ... ,2*ghost+n -1
                    V_ext[n + n_ghost + i, :] = V[n - 1, :]

        # dirichlet
        elif bc[0] == 'dirichlet' or bc[1] == 'dirichlet':
            if bc[0] == 'dirichlet':
                for i in range(n_ghost):
                    # 0, .. ghost-1,| ghost .... ghost + n - 1,| ghost + n ... ,2*ghost+n -1
                    V_ext[i, :] = bc[2]

            if bc[1] == 'dirichlet':
                for i in range(n_ghost):
                    # 0, .. ghost-1,| ghost .... ghost + n - 1,| ghost + n ... ,2*ghost+n -1
                    V_ext[n + n_ghost + i, :] = bc[3]
        else:
            print('unknow boundary condition', bc[0], ' ', bc[1])

        return V_ext


    def _reconstruct(self, V_ext):
        n = self._n
        n_eqn = self._n_eqn
        n_ghost = self._n_inv_ghost
        V_minus, V_plus = np.empty((n+1, n_eqn)), np.empty((n+1, n_eqn))


        for i in range(n+1):
            V_minus[i,:], V_plus[i,:] = self._reconstruct_func(V_ext[i: i + 2*n_ghost, :])

        return V_minus, V_plus



    def spatial_discretization(self, W, t):
        '''
        dV/dt = res
        :param self:
        :param W: n by num_eqn, conservative variables
        :param t:
        :return:
        '''

        app = self._app

        if(app.need_variable_change()):
            V = app.to_pri(W)
        else:
            V = W

        V_ext = self._populate_ghost(V, self._n_inv_ghost)

        V_minus, V_plus = self._reconstruct(V_ext)

        # convective flux
        res = app.invisid_term(V_minus, V_plus, t)

        # diffusive flux
        if(app.is_viscous()):

            V_ext = self._populate_ghost(V, self._n_vis_ghost)

            res += app.viscous_term(V_ext, t)

        # source term
        if (app.has_source()):
            res += app.source_term(V, t)

        return res

    def temporal_discretization(self, V, dt, t):
        '''
        :param V: conservative variables
        :param dt:
        :param t:
        :return:
        '''

        if self._temporal_scheme == 'ForwardEuler':

            dV = self.spatial_discretization(V, t)
            return V + dt * dV

        elif self._temporal_scheme == 'RK2':

            dV = self.spatial_discretization(V, t)

            V_temp = V +  dt * dV

            dV = self.spatial_discretization(V_temp, t + dt)

            V_temp += dt * dV

            return 0.5 * (V_temp + V)

        else:
            print('unknown scheme', self._temporal_scheme)


    def solve(self, V0):
        '''

        :param V0: initial condition, a n_ele by n_eqn array
        :return:
        '''
        app = self._app

        if(V0.ndim != 2):
            print(' convert V0 to 2d array' )
            V0 = V0.reshape((V0.shape[0],-1))

        V = V0

        t, n_step = 0., 0

        self._output(t, V0)

        while t < self._T:

            dt_cfl = app.max_dt(V, self._cfl)

            #Daniel Huang
            #dt_cfl = 0.0001

            dt_output = self._frame*self._output_dt - t

            dt = min(dt_cfl, dt_output)

            V = self.temporal_discretization(V, dt, t)

            t += dt

            n_step += 1

            if dt_output <= dt_cfl:
                self._output(t, V)

        return V

    def _output(self, t, V, var = 'solution'):
        if var == 'solution':
            print('output at time t ', t)
            fname = self._output_file + '_ele%06d'%self._n + '_frame%04d'%self._frame
            np.savetxt(fname, V, fmt = '%0.15E', header='%0.15E'%t)
            self._frame += 1

    def plot(self, V, label = 'y', fig_id = 0):
        n, dx = self._n, self._dx
        xx = (np.arange(n) + 0.5)*dx
        plt.figure(fig_id)
        plt.plot(xx, V, '-o')
        plt.xlabel('x')
        plt.ylabel(label)
        plt.show()


