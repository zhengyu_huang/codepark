import numpy as np

def constant(vv):
    return vv[0,:], vv[1,:]


def Van_Leer(vv):
    _, n_eqn = vv.shape
    v_minus, v_plus = np.empty(n_eqn), np.empty(n_eqn)
    eps = 1.e-15
    for i in range(n_eqn):
        v_ll, v_l, v_r, v_rr = vv[0, i], vv[1, i], vv[2, i], vv[3, i]

        ind_l = (v_r - v_l) / (v_l - v_ll + eps)
        ind_r = (v_r - v_l) / (v_rr - v_r + eps)


        phi_l = 2 * ind_l / (ind_l + 1.) if ind_l > 0. else 0.

        phi_r = 2 * ind_r / (ind_r + 1.) if ind_r > 0. else 0.

        v_minus[i] = v_l + 0.5 * phi_l * (v_l - v_ll)
        v_plus[i] = v_r - 0.5 * phi_r * (v_rr - v_r)

    return v_minus, v_plus
