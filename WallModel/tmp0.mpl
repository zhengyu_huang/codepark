_EnvUseHeavisideAsUnitStep := true:
`diff/abs` := proc(g,x) diff(g,x)*signum(g) end proc:
`diff/Heaviside` := proc(g,x) 0 end proc:
Digits:=20:with(linalg):  with(codegen):  with(LinearAlgebra):
Prt := 0.9:
kappa := 0.41:
Ap := 17.0:
yp := y*sqrt(du0*p/(mu0*R*T0)):
rho := p/(R*T):
tau_w := mu0*du0:
cp := gam*R/(gam - 1):
mut := kappa*y*sqrt(tau_w*rho)*(1 - exp(-yp/Ap))*(1 - exp(-yp/Ap)):
F1 := tau_w/(mu + mut):
F2 := (cp*mu0/Pr*dT0 - tau_w*u)/(cp*(mu/Pr + mut/Prt)):
dF1du := diff(F1, u):
dF2du := diff(F2, u):
dF1dT := diff(F1, T):
dF2dT := diff(F2, T):
dF1dmu := diff(F1, mu):
dF2dmu := diff(F2, mu):
dF1ddu0 := diff(F1, du0):
dF2ddu0 := diff(F2, du0):
dF1dT0 := diff(F1, T0):
dF2dT0 := diff(F2, T0):
dF1ddT0 := diff(F1, dT0):
dF2ddT0 := diff(F2, dT0):
dF1dmu0 := diff(F1, mu0):
dF2dmu0 := diff(F2, mu0):
dFdU:=evalm([dF1du, dF2du, dF1dT, dF2dT, dF1dmu, dF2dmu]):
dFdP:=evalm([dF1ddu0, dF2ddu0, dF1dT0, dF2dT0, dF1ddT0, dF2ddT0, dF1dmu0, dF2dmu0]):
U:='U':P:='P':y_glob:='y_glob':p_glob:='p_glob':gam_glob:='gam_glob':R_glob:='R_glob':Pr_glob:='Pr_glob':dFdUproc:=optimize(makevoid(makeproc(dFdU, [u, T, mu, du0, T0, dT0, mu0, y, p, gam, R, Pr, dFdU]))):
dFdPproc:=optimize(makevoid(makeproc(dFdP, [u, T, mu, du0, T0, dT0, mu0, y, p, gam, R, Pr, dFdP]))):
bc_RANS_deriv:=makevoid(joinprocs([dFdUproc, dFdPproc])):
bc_RANS_deriv:=packparams(bc_RANS_deriv, [u, T, mu], U):
bc_RANS_deriv:=packparams(bc_RANS_deriv, [du0, T0, dT0, mu0], P):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 7=8):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 8=9):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 6=7):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 7=8):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 5=6):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 6=7):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 4=5):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 5=6):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 3=4):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 4=5):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 2=3):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 3=4):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 1=2):
bc_RANS_deriv:=swapargs(bc_RANS_deriv, 2=3):
bc_RANS_deriv:=declare(U::('array'(1..3, numeric)), bc_RANS_deriv):
bc_RANS_deriv:=declare(P::('array'(1..4, numeric)), bc_RANS_deriv):
bc_RANS_deriv:=declare(y_glob::numeric, bc_RANS_deriv):
bc_RANS_deriv:=declare(p_glob::numeric, bc_RANS_deriv):
bc_RANS_deriv:=declare(gam_glob::numeric, bc_RANS_deriv):
bc_RANS_deriv:=declare(R_glob::numeric, bc_RANS_deriv):
bc_RANS_deriv:=declare(Pr_glob::numeric, bc_RANS_deriv):
bc_RANS_deriv:=declare(dFdU::('array'(1..6, numeric)), bc_RANS_deriv):
bc_RANS_deriv:=declare(dFdP::('array'(1..8, numeric)), bc_RANS_deriv):
CodeGeneration[C](bc_RANS_deriv, optimize, deducetypes=false, output="WallModel.cc"):