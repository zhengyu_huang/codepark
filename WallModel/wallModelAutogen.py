from autogen.util import declare_vec
from autogen.autog import autogen
from autogen.deriv import mk_deriv1_name, mk_deriv1_expr
from autogen.ops import *



def wall_model():
    # Assuming equilibrium, the unresolved inner layer is modeled by solving RANS boundary layer equation, in an overlapping
    # layer between y = 0 and y = hwm
    # check the equation in the paper "Wall-modeling in large eddy simulation: Length scales, grid resolution, and
    # accuracy" by Soshi Kawai, and Johan Larsson
    # the equation can be rewrite to ODE format
    # du/dy = f1(u,T, mu, du0, T0, dT0, mu0, y, p, gam, R, Pr)
    # dT/dy = f2(u,T, mu, du0, T0, dT0, mu0, y, p, gam, R, Pr)
    # F = [f1,f2]
    # This function, compute F and dF/dU, and dF/dP
    #:return:

    U = ('U',['u', 'T', 'mu'])                  #  U vector of velocity, temperature and dynamic viscosity
    P = ('P',['du0', 'T0', 'dT0', 'mu0'])       #  P vector of parameters, du/dy(y=0), T(y=0), dT/dy(y=0),  mu(y=0)
    
    F = declare_vec('F', 2) # 'F' right hand side

    expr = ['Prt = 0.9', 'kappa = 0.41', 'Ap = 17.0']
    
    expr += ['yp = y*sqrt(du0*p/(mu0*R*T0))', 'rho = p/(R*T)', 'tau_w = mu0*du0', 'cp = gam*R/(gam - 1)']

    expr += ['mut = kappa*y*sqrt(tau_w*rho)*(1 - exp(-yp/Ap))*(1 - exp(-yp/Ap))']

    expr += ['F1 = tau_w/(mu + mut)', 'F2 = (cp*mu0/Pr*dT0 - tau_w*u)/(cp*(mu/Pr + mut/Prt))']
    


    outvar, invar = [F], [U, P, 'y', 'p', 'gam', 'R', 'Pr']

    autogen(expr, outvar, invar, 'WallModel', 'C', func='bc_RANS', fname_mpl='tmp0.mpl', forceptr = ['y', 'p', 'gam', 'R', 'Pr'])



    # One derivative of F, G w.r.t. U
    dFdU = mk_deriv1_name(F, U)
    der1_expr = mk_deriv1_expr(F[1], U[1], dFdU[1])
    dFdP = mk_deriv1_name(F, P)
    der1_expr += mk_deriv1_expr(F[1], P[1], dFdP[1])
    
    # dvar: Dependent variables -- output variables and/or those to be diff
    # ivar: Independent variables -- input variables and/or diff w.r.t.
    # param: Parameters -- input variables not involved in differentiation
    outvar, invar = [dFdU, dFdP], [U, P, 'y',  'p', 'gam', 'R', 'Pr']

    # Call autogen to generate Maple code and run Maple to generate 'C' code;
    # generate Fortran, Python, Matlab, etc code by changing 'C'
    autogen(expr+der1_expr, outvar, invar, 'WallModel', 'C',
            func='bc_RANS_deriv', fname_mpl='tmp0.mpl', forceptr = ['y', 'p', 'gam', 'R' ,'Pr'])


if __name__ == "__main__":
    wall_model()
