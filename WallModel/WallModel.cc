#include <math.h>
void bc_RANS (
  double U[3],
  double P[4],
  double y,
  double p,
  double gam,
  double R,
  double Pr,
  double F[2])
{
  double t1;
  double t15;
  double t18;
  double t2;
  double t20;
  double t21;
  double t26;
  double t27;
  double t29;
  double t3;
  double t4;
  double t7;
  t3 = P[3];
  t4 = P[0];
  t1 = t3 * t4;
  t2 = 0.1e1 / R;
  t7 = sqrt(t1 * p * t2 / U[1]);
  t15 = sqrt(t4 * p / t3 * t2 / P[1]);
  t18 = exp(-0.58823529411764705882e-1 * y * t15);
  t20 = pow(0.1e1 - t18, 0.2e1);
  t21 = y * t7 * t20;
  t27 = U[2];
  F[0] = t1 / (t27 + 0.41e0 * t21);
  t26 = gam - 0.1e1;
  t29 = 0.1e1 / Pr;
  F[1] = (gam * R / t26 * t3 * t29 * P[2] - t1 * U[0]) / gam * t2 * t26 / (t27 * t29 + 0.45555555555555555556e0 * t21);
  return;
}

void bc_RANS_deriv (
  double U[3],
  double P[4],
  double y,
  double p,
  double gam,
  double R,
  double Pr,
  double dFdU[6],
  double dFdP[8])
{
  double t1;
  double t10;
  double t103;
  double t11;
  double t110;
  double t117;
  double t120;
  double t13;
  double t133;
  double t14;
  double t15;
  double t150;
  double t151;
  double t16;
  double t18;
  double t19;
  double t2;
  double t20;
  double t21;
  double t211;
  double t22;
  double t23;
  double t24;
  double t241;
  double t25;
  double t26;
  double t261;
  double t27;
  double t271;
  double t29;
  double t3;
  double t30;
  double t31;
  double t32;
  double t33;
  double t34;
  double t341;
  double t35;
  double t36;
  double t37;
  double t38;
  double t381;
  double t39;
  double t391;
  double t41;
  double t42;
  double t43;
  double t431;
  double t44;
  double t45;
  double t451;
  double t5;
  double t51;
  double t52;
  double t53;
  double t55;
  double t57;
  double t58;
  double t581;
  double t59;
  double t6;
  double t60;
  double t61;
  double t62;
  double t63;
  double t631;
  double t65;
  double t7;
  double t70;
  double t72;
  double t73;
  double t74;
  double t85;
  double t86;
  double t9;
  double t92;
  double t93;
  double t96;
  double t97;
  dFdU[0] = 0.0e0;
  t3 = P[3];
  t6 = P[0];
  t11 = t3 * t6;
  t22 = 0.1e1 / gam;
  t42 = 0.1e1 / R;
  t5 = gam - 0.1e1;
  t73 = 0.1e1 / Pr;
  t92 = p * t42;
  t14 = U[1];
  t16 = 0.1e1 / t14;
  t13 = sqrt(t11 * t92 * t16);
  t151 = t6 * p;
  t25 = 0.1e1 / t3;
  t31 = P[1];
  t35 = 0.1e1 / t31;
  t211 = sqrt(t151 * t25 * t42 * t35);
  t241 = exp(-0.58823529411764705882e-1 * y * t211);
  t261 = pow(0.1e1 - t241, 0.2e1);
  t271 = y * t13 * t261;
  t52 = U[2];
  t29 = t52 * t73 + 0.45555555555555555556e0 * t271;
  dFdU[1] = -t11 * t22 * t42 * t5 / t29;
  t33 = t3 * t3;
  t341 = t6 * t6;
  t381 = pow(t52 + 0.41e0 * t271, 0.2e1);
  t391 = 0.1e1 / t381;
  t431 = 0.1e1 / t13 * t261;
  t44 = t14 * t14;
  t451 = 0.1e1 / t44;
  dFdU[2] = 0.20500000000000000000e0 * t33 * t341 * t391 * y * t431 * t92 * t451;
  t93 = P[2];
  t97 = U[0];
  t57 = (gam * R / t5 * t3 * t73 * t93 - t11 * t97) * t22;
  t581 = R * R;
  t61 = t29 * t29;
  t631 = t5 / t61;
  dFdU[3] = 0.22777777777777777778e0 * t57 / t581 * t631 * y * t431 * t3 * t151 * t451;
  dFdU[4] = -t11 * t391;
  dFdU[5] = -t57 * t42 * t631 * t73;
  t1 = t11;
  t2 = t42;
  t117 = t2 * t16;
  t7 = sqrt(t1 * p * t117);
  t9 = t151;
  t10 = t25;
  t120 = t2 * t35;
  t15 = sqrt(t9 * t10 * t120);
  t18 = exp(-0.58823529411764705882e-1 * y * t15);
  t19 = 0.1e1 - t18;
  t20 = t19 * t19;
  t21 = y * t7 * t20;
  t23 = t52 + 0.41e0 * t21;
  t24 = 0.1e1 / t23;
  t26 = t23 * t23;
  t27 = 0.1e1 / t26;
  t30 = y / t7 * t20;
  t32 = t117;
  t34 = t30 * t3 * p * t32;
  t36 = y * y;
  t37 = t36 * t7;
  t38 = 0.1e1 / t15;
  t39 = t19 * t38;
  t41 = p * t10;
  t43 = t120 * t18;
  t45 = t37 * t39 * t41 * t43;
  t133 = t1 * t27;
  dFdP[0] = t3 * t24 - t133 * (0.20500000000000000000e0 * t34 + 0.24117647058823529412e-1 * t45);
  t51 = t22;
  t53 = t5;
  t55 = t73;
  t58 = t52 * t55 + 0.45555555555555555556e0 * t21;
  t59 = 0.1e1 / t58;
  t60 = t2 * t53 * t59;
  t62 = gam * R;
  t63 = 0.1e1 / t53;
  t65 = t3 * t55;
  t70 = (t62 * t63 * t65 * t93 - t1 * t97) * t51;
  t72 = t58 * t58;
  t74 = t53 / t72;
  t150 = t70 * t2 * t74;
  dFdP[1] = -t3 * t97 * t51 * t60 - t150 * (0.22777777777777777778e0 * t34 + 0.26797385620915032680e-1 * t45);
  t85 = t31 * t31;
  t86 = 0.1e1 / t85;
  dFdP[2] = 0.24117647058823529412e-1 * t341 * t27 * t37 * t19 * t38 * p * t2 * t86 * t18;
  t96 = t39 * t6;
  dFdP[3] = 0.26797385620915032680e-1 * t70 / t581 * t74 * t37 * t96 * t41 * t86 * t18;
  dFdP[4] = 0.0e0;
  dFdP[5] = t65 * t59;
  t103 = t30 * t9 * t32;
  t110 = t37 * t96 * p / t33 * t43;
  dFdP[6] = t6 * t24 - t133 * (0.20500000000000000000e0 * t103 - 0.24117647058823529412e-1 * t110);
  dFdP[7] = (t55 * t62 * t63 * t93 - t6 * t97) * t51 * t60 - t150 * (0.22777777777777777778e0 * t103 - 0.26797385620915032680e-1 * t110);
  return;
}
