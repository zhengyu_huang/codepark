import numpy as np
from WallModel import bc_RANS, bc_RANS_deriv
'''
Adiabatic wall
'''


def nonlinear_solver(x0, func, para, eps=1e-8, maxite=1000):
    '''
    Nonlinear newton solver,
    :param x0: initial guess, a 1D array
    :param func: solve func(x, para) = 0
    :param eps: stop criterion, |dx| < eps or |dx|/|x| < eps
    :param maxite: stop criterion max iteration
    :return:
    '''
    x = x0
    for i in range(maxite):
        f, df = func(x,para)
        dx = np.linalg.solve(df, f)
        x = x - dx
        if np.linalg.norm(dx) < eps  or np.linalg.norm(dx)/np.linalg.norm(x0) < eps:
            return x, (np.linalg.norm(dx),np.linalg.norm(dx)/np.linalg.norm(x0), i)

    print("ERROR: Nonlinear solver diverges, |dx| is ", np.linalg.norm(dx), ' |dx|/|x0| is ', np.linalg.norm(dx)/np.linalg.norm(x0),' expect eps is ', eps)
    return x, (np.linalg.norm(dx),np.linalg.norm(dx)/np.linalg.norm(x0), i)


def RK_solver(x0, func, t, N, para):
    '''
    optimal second order TVD Runge-Kutta method
    dx/dt = func(x,t, para)
    x(t[0]) = x0
    solve for x(t[1])
    :param n: divide time domain to n segments
    :return:

    '''

    def RK(x0, func, ta, tb, para):
        '''
        TVD RK
        x^ = x_n + dt*func(x_n, t, para)
        x_{n+1} =  0.5*(x_n + x^ + dt*func(x^, t + dt, para))
        '''
        x = x0 + (tb - ta) * func(x0, ta, para)
        return 0.5 * (x0 + x + (tb - ta) * func(x, tb, para))

    x = x0
    t0, tn = t
    dt = (tn - t0) / N
    for i in range(N):
        ta, tb = t0 + i * dt, t0 + (i + 1) * dt
        x = RK(x, func, ta, tb, para)

    return x


def mu(T):
    '''
    :param T: temperature
    :return: mu dynamic viscosity
    '''
    return 1./T


def dmu(T):
    '''
    :param T: temperature
    :return: dmu/dT: the derivative of the dynamic viscosity over temperature
    '''
    return -1./(T*T)


def adiabatic_bc_func(x, para):
    '''
    Assuming equilibrium, the unresolved inner layer is modeled by solving RANS boundary layer equation, in an overlapping
    layer between y = 0 and y = h
    check the equation in the paper "Wall-modeling in large eddy simulation: Length scales, grid resolution, and
    accuracy" by Soshi Kawai, and Johan Larsson
    the equation can be rewrite to ODE format
    du/dy = f1(u,T, mu, du0, T0, dT0, mu0, y, p, gam, R, Pr)
    dT/dy = f2(u,T, mu, du0, T0, dT0, mu0, y, p, gam, R, Pr)


    :param x: for adiabatic wall the parameters we need to solve are [du_dy0, T0],
    :param para: parameters, we know, dT_dy0; y; p(y); u(y); T(y); gam; R; Pr; N: N+1 points for RK integrator

    :return: solve the ODE, we can get u(y) and T(y)
             f  = u(y,...) - u, T(y, ...) - T
             df = df/dx
    '''
    du_dy0, T0 = x
    dT_dy0, y, p, u, T, gam, R, Pr, N = para

    #############################################
    # initial condition x0 = [u0,T0, du0/dp0, dT0/dp0, du0/dp1, dT0/dp1], here p0 = du_dy0, p1 = T0
    x0 = np.array([0.0, T0, 0.0, 0.0, 0.0, 1.0])
    t = [0, y]

    def func(x, y, para):
        [u, T, d1, d2, d3, d4] = x
        D = np.array([[d1, d3], [d2, d4]])
        [du0, T0, dT0, p, gam, R, Pr] = para
        mu0 = mu(T0)

        F = np.array([0., 0.])
        U = [u, T, mu(T)]
        P = [du0, T0, dT0, mu0]
        bc_RANS(U, P, y, p, gam, R, Pr, F)

        #dF = dF/d(du_dy0, T0) = \partial_F/\partial(u,T)*d(u,T)/d(du_dy0, T0) + \partial_F/\partial(du_dy0, T0)

        dFdU, dFdP = np.zeros(6), np.zeros(8)
        U = [u, T, mu(T)]
        P = [du0, T0, dT0, mu0]
        bc_RANS_deriv(U, P, y, p, gam, R, Pr, dFdU, dFdP)

        dFdU = np.reshape(dFdU, (2, 3), order='F')
        dUdu = np.array([[1.0, 0.0], [0.0, 1.0], [0.0, dmu(T)]])
        dFdP = np.reshape(dFdP, (2, 4), order='F')
        dPdp = np.array([[1., 0.], [0., 1.], [0., 0.], [0., dmu(T0)]])
        dF = np.dot(dFdU, np.dot(dUdu, D)) + np.dot(dFdP, dPdp)


        return np.array([F[0], F[1], dF[0, 0], dF[1, 0], dF[0, 1], dF[1, 1]])

    # test func


    para1 = [du_dy0, T0, dT_dy0, p, gam, R, Pr]

    cf = RK_solver(x0, func, t, N, para1)
    f = np.array([cf[0], cf[1]]) - np.array([u, T])
    df = np.array([[cf[2], cf[4]], [cf[3], cf[5]]])



    return f, df


def adiabatic_wall_model(p, u, T, y, dT_dy0, N):
    '''
    :param p : constant pressure
    :param u : velocity at the outer layer
    :param T : temperature at the outer layer
    :param y : distance to wall
    :param dT_dy0 : dT/dy(y=0), heat flux at the wall

    Assume we have u0, tau_w, T0, q_w
    param R, Pr,
    :return:
    '''
    # Flow parameters
    gam, R = 1.4, 285.0
    Pr, Pr_t = 0.72, 0.9


    # ODE parameters
    du_dy0, T0 = u / y, T  # guess
    x = np.array([du_dy0, T0])
    para = [dT_dy0, y, p, u, T, gam, R, Pr, N]

    return nonlinear_solver(x, adiabatic_bc_func, para)


def isothermal_bc_func(x, para):
    '''
    Assuming equilibrium, the unresolved inner layer is modeled by solving RANS boundary layer equation, in an overlapping
    layer between y = 0 and y = h
    check the equation in the paper "Wall-modeling in large eddy simulation: Length scales, grid resolution, and
    accuracy" by Soshi Kawai, and Johan Larsson
    the equation can be rewrite to ODE format
    du/dy = f1(u,T, mu, du0, T0, dT0, mu0, y, p, gam, R, Pr)
    dT/dy = f2(u,T, mu, du0, T0, dT0, mu0, y, p, gam, R, Pr)


    :param x: for isothermal wall the parameters we need to solve are [du_dy0, dT_dy0],
    :param para: parameters, we know, T0; y; p(y); u(y); T(y); gam; R; Pr; N: N+1 points for RK integrator

    :return: solve the ODE, we can get u(y) and T(y)
             f  = u(y,...) - u, T(y, ...) - T
             df = df/dx
    '''
    du_dy0, dT_dy0 = x
    T0, y, p, u, T, gam, R, Pr, N = para

    ############################################# #
    # initial condition x0 = [u0,T0, du0/dp0, dT0/dp0, du0/dp1, dT0/dp1], here p0 = du_dy0, p1 = dT_dy0
    x0 = np.array([0.0, T0, 0.0, 0.0, 0.0, 0.0])
    t = [0, y]

    def func(x, y, para):
        [u, T, d1, d2, d3, d4] = x
        D = np.array([[d1, d3], [d2, d4]])
        [du0, T0, dT0, p, gam, R, Pr] = para
        mu0 = mu(T0)

        F = np.array([0., 0.])
        U = [u, T, mu(T)]
        P = [du0, T0, dT0, mu0]
        bc_RANS(U, P, y, p, gam, R, Pr, F)

        # dF = dF/d(du_dy0, dT_dy0) = \partial_F/\partial(u,T)*d(u,T)/d(du_dy0, dT_dy0) + \partial_F/\partial(du_dy0, dT_dy0)

        dFdU, dFdP = np.zeros(6), np.zeros(8)
        U = [u, T, mu(T)]
        P = [du0, T0, dT0, mu0]
        bc_RANS_deriv(U, P, y, p, gam, R, Pr, dFdU, dFdP)

        dFdU = np.reshape(dFdU, (2, 3), order='F')
        dUdu = np.array([[1.0, 0.0], [0.0, 1.0], [0.0, dmu(T)]])
        dFdP = np.reshape(dFdP, (2, 4), order='F')
        dPdp = np.array([[1., 0.], [0., 0.], [0., 1.], [0., 0.]])
        dF = np.dot(dFdU, np.dot(dUdu, D)) + np.dot(dFdP, dPdp)

        return np.array([F[0], F[1], dF[0, 0], dF[1, 0], dF[0, 1], dF[1, 1]])



    para1 = [du_dy0, T0, dT_dy0, p, gam, R, Pr]


    cf = RK_solver(x0, func, t, N, para1)
    f = np.array([cf[0], cf[1]]) - np.array([u, T])
    df = np.array([[cf[2], cf[4]], [cf[3], cf[5]]])



    return f, df


def isothermal_wall_model(p, u, T, y, T0, N):
    '''
    :param p : constant pressure
    :param u : velocity at the outer layer
    :param T : temperature at the outer layer
    :param y : distance to wall
    :param T0 : wall temperature

    Assume we have u0, tau_w, T0, q_w
    param R, Pr,
    :return:
    '''
    # Flow parameters
    gam, R = 1.4, 285.0
    Pr, Pr_t = 0.72, 0.9

    # ODE parameters
    du_dy0, dT_dy0 = u / y, (T - T0)/y  # guess
    x = np.array([du_dy0, dT_dy0])
    para = [T0, y, p, u, T, gam, R, Pr, N]

    return nonlinear_solver(x, isothermal_bc_func, para)


if __name__ == "__main__":
    #### utility test
    eps = 1e-3

    print('test mu and dmu')
    T = 100
    print('eps is ', eps, 'finite difference error is ', mu(T+eps) - mu(T-eps) - dmu(T)*2.*eps)

    print('test adiabatic_bc_func and isothermal_bc_func')
    p = 1e4
    u = 100.0
    y = 1e-1
    N = 50
    gam, R = 1.4, 285.0
    Pr, Pr_t = 0.72, 0.9
    du_dy0 = 3333.3
    dT_dy0 = 1000.2
    T0 = 200.0

    x = np.array([du_dy0, T0])
    para = [dT_dy0, y, p, u, T, gam, R, Pr, N]

    eps_v = eps*np.array([1.0,2.0])
    fp, dfp = adiabatic_bc_func(x + eps_v, para)
    f, df = adiabatic_bc_func(x, para)
    fm, dfm = adiabatic_bc_func(x - eps_v, para)
    print('adiabatic_bc_func: eps is ', eps, 'O3 finite difference error is ', fp - fm - np.dot(df, 2. * eps_v))
    fm, dfm = adiabatic_bc_func(x, para)
    print('adiabatic_bc_func: eps is ', eps, 'O2 finite difference error is ', fp - fm - np.dot(df, eps_v))

    x = np.array([du_dy0, dT_dy0])
    para = [T0, y, p, u, T, gam, R, Pr, N]

    fp, dfp = isothermal_bc_func(x + eps_v, para)
    f, df = isothermal_bc_func(x, para)
    fm, dfm = isothermal_bc_func(x - eps_v, para)
    print('isothermal_bc_func: eps is ', eps, 'O3 finite difference error is ', fp - fm - np.dot(df, 2. * eps_v))
    fm, dfm = isothermal_bc_func(x, para)
    print('isothermal_bc_func: eps is ', eps, 'O finite difference error is ', fp - fm - np.dot(df, eps_v))

    p = 1e5
    u = 200.0
    T = 300.
    y = 1e-2
    q = 0.0
    N = 50
    ans = adiabatic_wall_model(p, u, T, y, q, N)
    print(ans)


    p = 1e5
    u = 200.0
    T = 300.
    y = 1e-2
    T0 = ans[0][1]
    N = 50
    ans = isothermal_wall_model(p, u, T, y, T0, N)
    print(ans)

    p = 1e5
    u = 200.0
    T = 300.
    y = 1e-2
    T0 = 400
    N = 50
    ans = isothermal_wall_model(p, u, T, y, T0, N)
    print(ans)

