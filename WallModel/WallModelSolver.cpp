#include <iostream>
#include <utility>

#include "WallModel.cc"
void nonlinear_solver(void (*func)(const double*, const double*, double*, double*), const double *para, double *x, const double eps=1e-8, const int maxite=1000){
    /*
    Nonlinear newton solver,
    :param x: initial guess, a 1D array of 2 elemenets!!!
    :param func: solve func(x, para) = 0
    :param eps: stop criterion, |dx| < eps or |dx|/|x| < eps
    :param maxite: stop criterion max iteration
    :return x
    */
    double dx[2], dx_norm, x0[2] = {x[0], x[1]}, x0_norm = sqrt(x0[0]*x0[0] + x0[1]*x0[1]);
    double f[2], df[4], detf;
    for(int i = 0; i < maxite; i++){
        func(x,para,f, df);
        detf = df[0] * df[3] - df[1] * df[2];
        dx[0] = ( df[3]*f[0] - df[2]*f[1])/detf;
        dx[1] = (-df[1]*f[0] + df[0]*f[1])/detf;
        x[0] -= dx[0];
        x[1] -= dx[1];
        dx_norm = sqrt(dx[0]*dx[0] + dx[1]*dx[1]);

        if(sqrt(dx[0]*dx[0] + dx[1]*dx[1]) < eps  || sqrt(dx[0]*dx[0] + dx[1]*dx[1])/sqrt(x0[0]*x0[0] + x0[1]*x0[1]) < eps)
            return;
    }

    std::cout <<  "ERROR: Nonlinear solver diverges, |dx| is " << dx_norm <<  " |dx|/|x0| is " <<  dx_norm/x0_norm << " expect eps is " <<  eps << std::endl;
}


void RK_solver(void (*func)(const double*, const double, const double*, double*), double* x,  const double* para,  const double* t, const int N = 50){
    /*
    optimal second order TVD Runge-Kutta method
    dx/dt = func(x,t, para)
    x(t[0]) = x0
    solve for x(t[1])
    :param n: divide time domain to n segments
    :return:

    */
    int nx = 6;
    double f[nx], xtemp[nx];
    double t0 = t[0], tn = t[1], ta, tb;
    double dt = (tn - t0) / N;
    for(int i =0; i < N; i++){
        ta = t0 + i * dt, tb = t0 + (i + 1) * dt;
        func(x, ta, para, f);
        for(int j =0; j < nx; j++)
            xtemp[j] = x[j] + (tb - ta) * f[j];
        func(xtemp, tb, para, f);
        for(int j =0; j < nx; j++)
            x[j] = 0.5 * (x[j] + xtemp[j] + (tb - ta) * f[j]);
        

    }

}



double mu(double T) {
    /*
    :param T: temperature
    :return: mu dynamic viscosity
    */
    return 1./T;
}

double dmu(double T) {
    /*
    :param T: temperature
    :return: dmu/dT: the derivative of the dynamic viscosity over temperature
    */
    return -1./(T*T);
}

void adiabatic_bc_func(const double* x, const double* para, double* f, double* df) {
    /*
    Assuming equilibrium, the unresolved inner layer is modeled by solving RANS boundary layer equation, in an overlapping
    layer between y = 0 and y = h
    check the equation in the paper "Wall-modeling in large eddy simulation: Length scales, grid resolution, and
    accuracy" by Soshi Kawai, and Johan Larsson
    the equation can be rewrite to ODE format
    du/dy = f1(u,T, mu, du0, T0, dT0, mu0, y, p, gam, R, Pr)
    dT/dy = f2(u,T, mu, du0, T0, dT0, mu0, y, p, gam, R, Pr)


    :param x: for adiabatic wall the parameters we need to solve are [du_dy0, T0],
    :param para: parameters, we know, dT_dy0; y; p(y); u(y); T(y); gam; R; Pr; N: N+1 points for RK integrator

    :return: solve the ODE, we can get u(y) and T(y)
             f  = u(y,...) - u, T(y, ...) - T
             df = df/dx
    */
    double du_dy0 = x[0], T0 = x[1];
    double dT_dy0 = para[0], y = para[1], p = para[2], u = para[3], T = para[4], gam = para[5], R = para[6], Pr = para[7];

    // initial condition x0 = [u0,T0, du0/dp0, dT0/dp0, du0/dp1, dT0/dp1], here p0 = du_dy0, p1 = T0
    double x0[6] = {0.0, T0, 0.0, 0.0, 0.0, 1.0};
    double t[2] = {0, y};

    auto func = [](const double *x, const double y, const double *para, double *f) {
        double u = x[0], T = x[1], d1 = x[2], d2 = x[3], d3 = x[4], d4 = x[5];
        double du0 = para[0], T0 = para[1], dT0 = para[2], p = para[3], gam = para[4], R = para[5], Pr = para[6];
        double mu0 = mu(T0), dmuT0 = dmu(T0), dmuT = dmu(T);

        double F[2] = {0., 0.};
        double dFdU[6], dFdP[8];
        double U[3] = {u, T, mu(T)};
        double P[4] = {du0, T0, dT0, mu0};
        bc_RANS(U, P, y, p, gam, R, Pr, F);

        //dF = dF/d(du_dy0, T0) = \partial_F/\partial(u,T,mu)*d(u,T,mu)/d(du_dy0, T0) + \partial_F/\partial(du_dy0, T0)

        bc_RANS_deriv(U, P, y, p, gam, R, Pr, dFdU, dFdP);

        double dF[4] = {dFdU[0] * d1 + dFdU[2] * d2 + dFdU[4] * d2 * dmuT + dFdP[0],
                        dFdU[1] * d1 + dFdU[3] * d2 + dFdU[5] * d2 * dmuT + dFdP[1],
                        dFdU[0] * d3 + dFdU[2] * d4 + dFdU[4] * d4 * dmuT + dFdP[2] + dFdP[6] * dmuT0,
                        dFdU[1] * d3 + dFdU[3] * d4 + dFdU[5] * d4 * dmuT + dFdP[3] + dFdP[7] * dmuT0};

        f[0] = F[0];
        f[1] = F[1];
        f[2] = dF[0];
        f[3] = dF[1];
        f[4] = dF[2];
        f[5] = dF[3];
    };


    double para1[7] = {du_dy0, T0, dT_dy0, p, gam, R, Pr};

    RK_solver(func, x0, para1, t);
    f[0] = x0[0] - u;
    f[1] = x0[1] - T;
    df[0] = x0[2];
    df[1] = x0[3];
    df[2] = x0[4];
    df[3] = x0[5];
}




void adiabatic_wall_model(const double p, const double u, const double T, const double y,
                          const double dT_dy0, double *x) {
    /*
    :param p : constant pressure
    :param u : velocity at the outer layer
    :param T : temperature at the outer layer
    :param y : distance to wall
    :param dT_dy0 : dT/dy(y=0), heat flux at the wall

    Assume we have u0, tau_w, T0, q_w
    param R, Pr,
    :return: x =  {du_dy0, T0};
    */
    // Flow parameters
    double gam = 1.4, R = 285.0;
    double Pr = 0.72, Pr_t = 0.9;


    // ODE parameters
    double du_dy0 = u / y, T0 = T;  // initial guess
    x[0] = du_dy0, x[1] = T0;
    double para[8] = {dT_dy0, y, p, u, T, gam, R, Pr};

    nonlinear_solver(adiabatic_bc_func, para, x);
    std::cout << "adiabatic wall " << x[0] << " " << x[1] << std::endl;
}


void isothermal_bc_func(const double* x, const double* para, double* f, double* df) {
    /*
    Assuming equilibrium, the unresolved inner layer is modeled by solving RANS boundary layer equation, in an overlapping
    layer between y = 0 and y = h
    check the equation in the paper "Wall-modeling in large eddy simulation: Length scales, grid resolution, and
    accuracy" by Soshi Kawai, and Johan Larsson
    the equation can be rewrite to ODE format
    du/dy = f1(u,T, mu, du0, T0, dT0, mu0, y, p, gam, R, Pr)
    dT/dy = f2(u,T, mu, du0, T0, dT0, mu0, y, p, gam, R, Pr)


    :param x: for isothermal wall the parameters we need to solve are [du_dy0, dT_dy0],
    :param para: parameters, we know, T0; y; p(y); u(y); T(y); gam; R; Pr; N: N+1 points for RK integrator

    :return: solve the ODE, we can get u(y) and T(y)
             f  = u(y,...) - u, T(y, ...) - T
             df = df/dx
    */
    double du_dy0 = x[0], dT_dy0 = x[1];
    double T0 = para[0], y = para[1], p = para[2], u = para[3], T = para[4],
            gam = para[5], R = para[6], Pr = para[7];


    // initial condition x0 = [u0,T0, du0/dp0, dT0/dp0, du0/dp1, dT0/dp1], here p0 = du_dy0, p1 = dT_dy0
    double x0[6] = {0.0, T0, 0.0, 0.0, 0.0, 0.0};
    double t[2] = {0, y};

    auto func = [](const double *x, const double y, const double *para, double *f) {
        double u = x[0], T = x[1], d1 = x[2], d2 = x[3], d3 = x[4], d4 = x[5];
        double du0 = para[0], T0 = para[1], dT0 = para[2], p = para[3], gam = para[4], R = para[5], Pr = para[6];
        double mu0 = mu(T0), dmuT0 = dmu(T0), dmuT = dmu(T);


        double F[2] = {0., 0.};
        double dFdU[6], dFdP[8];
        double U[3] = {u, T, mu(T)};
        double P[4] = {du0, T0, dT0, mu0};

        bc_RANS(U, P, y, p, gam, R, Pr, F);

        // dF = dF/d(du_dy0, dT_dy0) = \partial_F/\partial(u,T)*d(u,T)/d(du_dy0, dT_dy0) + \partial_F/\partial(du_dy0, dT_dy0)

        bc_RANS_deriv(U, P, y, p, gam, R, Pr, dFdU, dFdP);

        double dF[4] = {dFdU[0] * d1 + dFdU[2] * d2 + dFdU[4] * d2 * dmuT + dFdP[0],
                        dFdU[1] * d1 + dFdU[3] * d2 + dFdU[5] * d2 * dmuT + dFdP[1],
                        dFdU[0] * d3 + dFdU[2] * d4 + dFdU[4] * d4 * dmuT + dFdP[4],
                        dFdU[1] * d3 + dFdU[3] * d4 + dFdU[5] * d4 * dmuT + dFdP[5]};

        f[0] = F[0];
        f[1] = F[1];
        f[2] = dF[0];
        f[3] = dF[1];
        f[4] = dF[2];
        f[5] = dF[3];
    };


    double para1[7] = {du_dy0, T0, dT_dy0, p, gam, R, Pr};


    RK_solver(func, x0, para1, t);

    f[0] = x0[0] - u;
    f[1] = x0[1] - T;
    df[0] = x0[2];
    df[1] = x0[3];
    df[2] = x0[4];
    df[3] = x0[5];
}


void isothermal_wall_model(const double p, const double u, const double T, const double y, const double T0, double* x){
    /*
    :param p : constant pressure
    :param u : velocity at the outer layer
    :param T : temperature at the outer layer
    :param y : distance to wall
    :param T0 : wall temperature

    Assume we have u0, tau_w, T0, q_w
    param R, Pr,
    :return: x = {du_dy0; x[1] = dT_dy0;}
    */
    // Flow parameters
    double gam = 1.4, R = 285.0;
    double Pr = 0.72, Pr_t = 0.9;

    // ODE parameters
    double du_dy0 = u/y,  dT_dy0  = (T - T0)/y; // guess
    x[0] = du_dy0; x[1] = dT_dy0;
    double para[8] = {T0, y, p, u, T, gam, R, Pr};

    nonlinear_solver(isothermal_bc_func, para, x);
    std::cout << "isothermal_wall " << x[0] << " " << x[1] << std::endl;
}

int main() {
    // utility test
    double eps = 1e-3;


    double T = 100;

    std::cout << "test adiabatic_bc_func and isothermal_bc_func" << std::endl;
    double p = 1e4, u = 100.0, y = 1e-1, N = 100, gam = 1.4, R = 285.0, Pr = 0.72,
            Pt_r = 0.9, du_dy0 = 3333.3, dT_dy0 = 1000.2, T0 = 200.0;

    double x[2] = {du_dy0, T0};
    double para[8] = {dT_dy0, y, p, u, T, gam, R, Pr};


    double eps_v[2] = {eps, 2 * eps};
    double x_p_eps_v[2] = {x[0] + eps, x[1] + 2 * eps};
    double f[2], df[4], fp[2], dfp[4], fm[2], dfm[4];
    adiabatic_bc_func(x_p_eps_v, para, fp, dfp);
    adiabatic_bc_func(x, para, f, df);
    double x_m_eps_v[2] = {x[0] - eps, x[1] - 2 * eps};
    adiabatic_bc_func(x_m_eps_v, para, fm, dfm);
    std::cout << " adiabatic_bc_func: eps is " << eps << " finite difference error is "
              << fp[0] - fm[0] - 2. * df[0] * eps_v[0] - 2. * df[2] * eps_v[1] << " "
              << fp[1] - fm[1] - 2. * df[1] * eps_v[0] - 2. * df[3] * eps_v[1] << std::endl;



    p = 1e5;
    u = 200.0;
    T = 300.;
    y = 1e-2;
    double q = 0.0;

    adiabatic_wall_model(p, u, T, y, q, x);


    p = 1e5;
    u = 200.0;
    T = 300.;
    y = 1e-2;
    q = 0.0;
    adiabatic_wall_model(p, u, T, y, q, x);



    p = 1e5;
    u = 200.0;
    T = 300.;
    y = 1e-2;
    T0 = x[1];

    isothermal_wall_model(p, u, T, y, T0, x);


    p = 1e5;
    u = 200.0;
    T = 300.;
    y = 1e-2;
    T0 = 400;
    isothermal_wall_model(p, u, T, y, T0, x);


    return 0;
}


