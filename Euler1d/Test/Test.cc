#include <iostream>
#include <cmath>
#include <math.h>
#include"../euler1d.cc"
#include <stdlib.h>


void prim2con(double* V, double*U, double gamma, int dim){
	U[0] = V[0];
	U[dim+1] = V[dim+1]/(gamma - 1);
	for(int i = 1 ; i < dim+1; i++){
		U[i] = V[i]*V[0];
		U[dim+1] += V[0]*V[i]*V[i]/2.0;
	}

}
void Euler1d_Verification(){
	int n = 6, dim = 1;
	double gamma = 1.4;
	double g[n] = {1.0, 1., 1., 1.,1., 1.};
	double v_G[n] = {0.0, 0, 0, 0., 0., 0. };

	double input_l[n*(dim + 2)] ={ 1.0, 1.0,2.0 ,
							 1.0, 0.75, 1.0,
							 1.0, -2.0, 0.4,
							 1.0, 0.0, 1000.0,
							 5.99924, 19.5975, 460.894,
							 1.0, 19.59745, 1000.0};

	double input_r[n*(dim+2)] ={
			1.0, 1.0, 2.0,
			0.125, 0.0, 0.1,
			1.0, 2.0, 0.4,
			1.0, 0.0, 0.01,
			5.99242, -6.19633, 46.0950,
			1.0, 19.59745, 0.01};

	double U_l[dim+2], U_r[dim+2],V_l[dim+2], V_r[dim+2], F[dim+2];
	for(int i = 0 ; i < n; i++){
		for(int j = 0; j < dim+2;j++){
			V_l[j] = input_l[i*(dim+2) + j];
			V_r[j] = input_r[i*(dim+2) + j];
		}

		prim2con(V_l, U_l, gamma, dim);
		prim2con(V_r, U_r, gamma, dim);

		roe1d_flux(U_l,U_r, g[i], v_G[i], 1.0,gamma,F);
		std::cout << i << " U_l "<< U_l[0] << " " << U_l[1] << " " << U_l[2] << std::endl;
		std::cout << i << " U_r "<< U_r[0] << " " << U_r[1] << " " << U_r[2] << std::endl;
		std::cout << i << " F   "<< F[0] << " " << F[1] << " " << F[2] << std::endl;

		wall1d_flux(U_l, g[i], v_G[i], 1.0,gamma,F);
		std::cout << i << " F_wall_l   "<< F[0] << " " << F[1] << " " << F[2] << std::endl;
		wall1d_flux(U_r, g[i], v_G[i], 1.0,gamma,F);
		std::cout << i << " F_wall_r   "<< F[0] << " " << F[1] << " " << F[2] << std::endl;
	
	}

	double g0 = 2.0;
	double v_G0 = 3.0;
	V_l[0] = 1.0; V_l[1] = 2.0; V_l[2] = 3.0;
	V_r[0] = V_l[0]; V_r[1] = v_G0; V_r[2] = V_l[2];
	prim2con(V_l, U_l, gamma, dim);
	prim2con(V_r, U_r, gamma, dim);

	roe1d_flux(U_l,U_r, g0, v_G0, -1.0,gamma,F);
	std::cout <<  " U_l "<< U_l[0] << " " << U_l[1] << " " << U_l[2] << std::endl;
	std::cout <<  " U_r "<< U_r[0] << " " << U_r[1] << " " << U_r[2] << std::endl;
	std::cout <<  " F   "<< F[0] << " " << F[1] << " " << F[2] << std::endl;

	wall1d_flux(U_l, g0, v_G0, -1.0,gamma,F);
	std::cout <<  " F_wall_l   "<< F[0] << " " << F[1] << " " << F[2] << std::endl;

}

void Euler1d_Deriv_Verification(){
	int n = 6, dim = 1;
	double gamma = 1.4;
	double g[n] = {1.0,2.0,3.0,1.0,2.0,3.0};
	double v_G[n] = {2.0,2.0,3.0,1.0,2.0,3.0};

	double input_l[n*(dim + 2)] ={
	                                                 1.0, 1.0,2.0 ,
							 1.0, 0.75, 1.0,
							 1.0, -2.0, 0.4,
							 1.0, 0.0, 1000.0,
							 5.99924, 19.5975, 460.894,
							 1.0, 19.59745, 1000.0};

	double input_r[n*(dim+2)] ={
			1.0, 1.0, 2.0,
			0.125, 0.0, 0.1,
			1.0, 2.0, 0.4,
			1.0, 0.0, 0.01,
			5.99242, -6.19633, 46.0950,
			1.0, 19.59745, 0.01};

	



	double U_l[dim+2], U_r[dim+2],U_lf[dim+2], U_rf[dim+2],U_lb[dim+2], U_rb[dim+2],
	  V_l[dim+2], V_r[dim+2], Ff[dim+2], Fb[dim+2], dF[3], dFNdU_lX[9],dFNdU_rX[9],dFNdg[3],dFNdv_G[3];
	double eps = 1e-6;
	double dU[dim+2] = {1., 1., 2.};
	double dg = 1.0;
	
	for(int i = 0 ; i < n; i++){

	   for(int j = 0; j < dim+2;j++){
	     //V_l[j] = input_l[i*(dim+2) + j];
	     //V_r[j] = input_r[i*(dim+2) + j];
	     
	      V_l[j] = ((double) rand()/(RAND_MAX)); //input_l[i*(dim+2) + j];
	      V_r[j] = ((double) rand()/(RAND_MAX));//input_r[i*(dim+2) + j];
	  	}
	       

		prim2con(V_l, U_l, gamma, dim);
		prim2con(V_r, U_r, gamma, dim);


		//test roe1d_flux_deriv
		for(int j=0;j < dim +2; j++){
		  U_lf[j] = U_l[j] + eps*dU[j];
		  U_lb[j] = U_l[j] - eps*dU[j];
		  
		}
		
		roe1d_flux(U_lf,U_r, g[i], v_G[i], 1.0,gamma,Ff);

		roe1d_flux(U_lb,U_r, g[i], v_G[i], 1.0,gamma,Fb);

		roe1d_flux_deriv(U_l,U_r,g[i],v_G[i], 1.0, gamma, dFNdU_lX,dFNdU_rX,dFNdg,dFNdv_G);

		for(int k=0;k < dim +2; k++){
		  dF[k] = (Ff[k] - Fb[k])/(2*eps);
		  for(int l=0; l < dim + 2; l++){
		    dF[k] -= dFNdU_lX[k + 3 * l]*dU[l];
		  }
		  
		}
		

		std::cout << " dF   "<< dF[0] << " " << dF[1] << " " << dF[2] << std::endl;
	
	}

}



int main(){
	Euler1d_Deriv_Verification();
	return 0;
}
