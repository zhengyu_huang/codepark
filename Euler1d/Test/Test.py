import numpy as np

def _wall_flux(prim_l, n_ij, gamma):
    [rho,vx,vy,p] = prim_l
    return np.array([0.0, p*n_ij[0] , p*n_ij[1], 0.0],dtype=float)

def _physical_flux(prim_l, n_ij, gamma):
    [rho,vx,vy,p] = prim_l
    e = p/(gamma-1) + 0.5*rho*(vx**2 + vy**2)
    return np.array([rho*(vx*n_ij[0]+ vy*n_ij[1]) , rho*vx*vx*n_ij[0] + rho*vx*vy*n_ij[1] + p*n_ij[0] ,
                     rho*vy*vy*n_ij[1] + p*n_ij[1] + rho*vx*vy*n_ij[0], (e + p)*(vx *n_ij[0] + vy*n_ij[1]) ],dtype=float)

def _Roe_flux(prim_l, prim_r, n_ij, gamma):
    n_len = np.sqrt(n_ij[0]**2 + n_ij[1]**2)
    n_ij = n_ij/n_len



    t_ij = np.array([-n_ij[1],n_ij[0]],dtype = float)


    # left state
    [rho_l, u_l, v_l, p_l] = prim_l;
    un_l = u_l*n_ij[0] + v_l*n_ij[1]
    ut_l = u_l*t_ij[0] + v_l*t_ij[1]
    a_l = np.sqrt(gamma*p_l/rho_l)
    h_l = 0.5*(v_l*v_l + u_l*u_l) + gamma * p_l/(rho_l * (gamma - 1.0));

    # right state
    [rho_r, u_r, v_r, p_r] = prim_r;
    un_r = u_r*n_ij[0] + v_r*n_ij[1]
    ut_r = u_r*t_ij[0] + v_r*t_ij[1]
    a_r = np.sqrt(gamma*p_r/rho_r)
    h_r = 0.5*(v_r*v_r + u_r*u_r) + gamma * p_r/(rho_r * (gamma - 1.0));

    # compute the Roe-averaged quatities
    RT = np.sqrt(rho_r/rho_l)
    rho_rl = RT * rho_l

    u_rl = (u_l + RT*u_r)/(1.0 + RT)

    v_rl = (v_l + RT*v_r)/(1.0 + RT)
    h_rl = (h_l + RT*h_r)/(1.0 + RT)
    a_rl = np.sqrt((gamma - 1)*(h_rl - 0.5*(u_rl*u_rl + v_rl*v_rl)))
    un_rl = u_rl*n_ij[0] + v_rl*n_ij[1]
    ut_rl = u_rl*t_ij[0] + v_rl*t_ij[1]


    # wave strengths
    dp = p_r - p_l
    drho = rho_r - rho_l
    dun = un_r - un_l
    dut = ut_r -ut_l
    du = np.array([(dp - rho_rl*a_rl*dun)/(2.0*a_rl*a_rl),  rho_rl*dut,  drho - dp/(a_rl * a_rl),  (dp + rho_rl*a_rl*dun)/(2.0*a_rl*a_rl)],dtype=float)

    #compute the Roe-average wave speeds
    ws = np.array([np.fabs(un_rl - a_rl), np.fabs(un_rl), np.fabs(un_rl), np.fabs(un_rl + a_rl)],dtype = float)

    #Harten's Entropy Fix JCP(1983), 49, pp357-393:
    # only for the nonlinear fields.
    '''
    dws = 1.0/5.0
    if ( ws[0] < dws ):
        ws[0] = 0.50 * ( ws[0]*ws[0]/dws + dws )
    if ( ws[3] < dws ):
        ws[3] = 0.50 * ( ws[3]*ws[3]/dws + dws )
    '''

    #compute the right characteristic eigenvectors
    P_inv = np.array([[1.0,                     0.0,           1.0,                             1.0],
                       [u_rl - a_rl*n_ij[0],    t_ij[0],       u_rl ,                           u_rl + a_rl*n_ij[0]],
                       [v_rl - a_rl*n_ij[1],    t_ij[1] ,      v_rl ,                           v_rl + a_rl*n_ij[1]],
                       [h_rl - un_rl*a_rl,      ut_rl,         0.5*(u_rl*u_rl+v_rl*v_rl),       h_rl + un_rl*a_rl]],dtype=float)


    f_l = np.array([rho_l*un_l, rho_l*un_l*u_l + p_l*n_ij[0],  rho_l*un_l*v_l + p_l*n_ij[1], rho_l*h_l*un_l ])
    f_r = np.array([rho_r*un_r, rho_r*un_r*u_r + p_r*n_ij[0],  rho_r*un_r*v_r + p_r*n_ij[1], rho_r*h_r*un_r ])

    flux = 0.5*(f_r + f_l  - np.dot(P_inv, du*ws))


    return n_len*flux

if __name__ == "__main__":
    input_l = np.array([
        [1.0, 1.0,2.0],
        [1.0, 0.75, 1.0],
        [1.0, -2.0, 0.4],
        [1.0, 0.0, 1000.0],
        [5.99924, 19.5975, 460.894],
        [1.0, 19.59745, 1000.0]
    ])
    # right primitive states input_l[i:,0:3]
    input_r = np.array([
        [1.0, 1.0, 2.0],
        [0.125, 0.0, 0.1],
        [1.0, 2.0, 0.4],
        [1.0, 0.0, 0.01],
        [5.99242, -6.19633, 46.0950],
        [1.0, 19.59745, 0.01]
    ])

    n = len(input_l)
    normal = [1.0,0.0]
    gamma = 1.4
    for i in range(n):
        v_l = [input_l[i,0],input_l[i,1], 0.0, input_l[i,2]]
        v_r = [input_r[i,0],input_r[i,1], 0.0, input_r[i,2]]
        print('Roe Flux', _Roe_flux(v_l, v_r, normal, gamma))
        print('wall Flux left', _physical_flux(v_l, normal, gamma))
        print('wall Flux right', _physical_flux(v_l, normal, gamma))

