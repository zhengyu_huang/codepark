from autogen.util import declare_vec
from autogen.autog import autogen
from autogen.deriv import mk_deriv1_name, mk_deriv1_expr
from autogen.ops import *


'''
def wall1d_flux():

    #Generate 1D wall flux function wall1d_flux and its derivative wall1d_flux_deriv in euler1d.cc
    #F(U_x/g) - U_x/g *v_G
    #U_x are conservative variables in reference coordinate, g is mesh motion dx/dX, v_G is mesh velocity
    #:return:


    UX = ('UX', ['rhoX', 'rhouX', 'eX']) # 'UX' vector in reference coordinate
    U = ('U',['rho', 'rhou', 'e'])       #  U vector in deformed coordinate
    F = declare_vec('F', 3) # 'F' wall flux


    expr = ['rho = rhoX/g', 'rhou = rhouX/g', 'e = eX/g']

    expr += ['p = (gam-1)*(e - rhou*rhou/(2*rho))',
             'F1 = (rhou  - rho*v_G)*N', 'F2 = (rhou*rhou/rho + p - rhou*v_G)*N','F3 = ((e+p)*(rhou/rho) - e*v_G)*N']


    outvar, invar = [F], [UX, 'g', 'v_G', 'N', 'gam']

    autogen(expr, outvar, invar, 'euler1d', 'C', func='wall1d_flux', fname_mpl='tmp0.mpl', forceptr = ['g', 'v_G' , 'N', 'gamma'])



    # One derivative of F, G w.r.t. U
    dFdUX = mk_deriv1_name(F, UX)
    der1_expr = mk_deriv1_expr(F[1], UX[1], dFdUX[1])
    dFdg = mk_deriv1_name(F, 'g')
    der1_expr += mk_deriv1_expr(F[1], 'g', dFdg[1])
    dFdv_G = mk_deriv1_name(F, 'v_G')
    der1_expr += mk_deriv1_expr(F[1], 'v_G', dFdv_G[1])

    # dvar: Dependent variables -- output variables and/or those to be diff
    # ivar: Independent variables -- input variables and/or diff w.r.t.
    # param: Parameters -- input variables not involved in differentiation
    outvar, invar = [dFdUX, dFdg, dFdv_G], [UX, 'g', 'v_G','N', 'gam']

    # Call autogen to generate Maple code and rhoun Maple to generate 'C' code;
    # generate Fortran, Python, Matlab, etc code by changing 'C'
    autogen(expr+der1_expr, outvar, invar, 'euler1d', 'C',
            func='wall1d_flux_deriv', fname_mpl='tmp0.mpl', forceptr = ['g', 'v_G' ,'N', 'gam'])
'''

'''
def wall1d_flux():
    #    http://chimeracfd.com/programming/gryphon/fluxroe.html
    #    Generate 1D Wall flux function through roe flux, and its derivative wall1d_flux_deriv in euler1d.cc
    #    F(U_x/g) - U_x/g *v_G
    #    U_lX are conservative variables in reference coordinate, g is mesh motion dx/dX, v_G is mesh velocity
    #    F_ROE = 0.5*(F(U_l)- U_lv_G + F(U_r)- U_rv_G) - 0.5|A_ROE - v_GI|(U_r - U_l)
    #    U_r = [rho_l, V_G, p_l]
    #    :return:


    # shift the velocity by v_G, compute flux

    U_lX = ('U_lX', ['rho_lX', 'rhou_lX', 'e_lX'])  # 'UX' conservative in reference domain
    U_l = ('U_l', ['rho_l', 'rhou_l', 'e_l'])  # 'U' conservative in deformed domain
    V_l = ('V_l', ['rho_l', 'u_l', 'p_l'])  # 'V' primitive in deformed domain
    expr = ['rho_l = rho_lX/g', 'rhou_l = rhou_lX/g', 'e_l = e_lX/g',
            'u_l = rhou_l/rho_l', 'p_l = (gam - 1)*(e_l - rhou_l*rhou_l/(2*rho_l))']

    # Define vector/matrix name and the name of its components
    V_r = ('V_r', ['rho_r', 'u_r', 'p_r'])
    expr += ['rho_r = rho_l', 'u_r = v_G', 'p_r = p_l','e_r = 0.5*rho_r*u_r*u_r + p_r/(gam-1)']

    F_ave = declare_vec('F_ave', 3)  # 'F' vector name; 'F1', ..., 'F4' components

    expr += ['F_ave1 = 0.5*(rho_r*u_r + rhou_l  - rho_l*v_G - rho_r*v_G)',
             'F_ave2 = 0.5*(rho_r*u_r*u_r + p_r  + rhou_l*rhou_l/rho_l + p_l - rho_r*u_r*v_G - rhou_l*v_G)',
             'F_ave3 = 0.5*((e_l+p_l)*u_l + (e_r+p_r)*u_r - e_l*v_G - e_r*v_G)']

    dV = ('dV', ['drho', 'du', 'dp'])

    expr += ['drho = rho_r -rho_l', 'du = u_r - u_l', 'dp = p_r - p_l']
    expr += ['h_l = (e_l + p_l)/rho_l', 'h_r = (e_r + p_r)/rho_r']

    # rho average of sound speed
    expr += ['rho_rl = sqrt(rho_r *rho_l)',
             'u_rl = (sqrt(rho_l)*u_l + sqrt(rho_r)*u_r)/(sqrt(rho_l) + sqrt(rho_r))',
             'h_rl = (sqrt(rho_l)*h_l + sqrt(rho_r)*h_r)/(sqrt(rho_l) + sqrt(rho_r))']

    # roe average of sound speed
    expr += ['a_rl = sqrt((gam-1) *(h_rl - 0.5*u_rl*u_rl))']

    P_inv = ['1', 'u_rl', '0.5*u_rl*u_rl',
             'rho_rl/(2*a_rl)', 'rho_rl/(2*a_rl)*(u_rl + a_rl)', 'rho_rl/(2*a_rl)*(h_rl + u_rl *a_rl)',
             '-rho_rl/(2*a_rl)', '-rho_rl/(2*a_rl)*(u_rl - a_rl)', '-rho_rl/(2*a_rl)*(h_rl - u_rl *a_rl)']

    Lam_Abs = declare_vec('Lam_Abs', 3)
    expr += ['Lam_Abs1 = abs(u_rl - v_G)', 'Lam_Abs2 = abs(u_rl + a_rl - v_G)', 'Lam_Abs3 = abs(u_rl - a_rl - v_G)']

    dU = declare_vec('dU', 3)
    expr += ['dU1 = drho - dp/(a_rl*a_rl)', 'dU2 = du + dp/(rho_rl*a_rl)', 'dU3 =  du - dp/(rho_rl*a_rl)']

    mult = declare_vec('mult', 3)
    expr += ['mult1 = Lam_Abs1*dU1', 'mult2 = Lam_Abs2*dU2', 'mult3 =  Lam_Abs3*dU3']
    # F = 0.5(F_l + F_r - P_inv * Lam_Abs * dU)

    F = declare_vec('F', 3)  # 'F' Roe flux


    expr += gemv((3, 3), -0.5, P_inv, mult[1], 1.0, F_ave[1], F[1])

    FN = declare_vec('FN', 3)  # 'F' multiplies normal N

    expr += scal('N', F[1], FN[1])

    outvar, invar = [FN], [U_lX, 'g', 'v_G', 'N', 'gam']

    autogen(expr, outvar, invar, 'euler1d', 'C', func='wall1d_flux', fname_mpl='tmp0.mpl',
            forceptr=['g', 'v_G', 'N', 'gam'])

    # One derivative of F, G w.r.t. U
    dFNdU_lX = mk_deriv1_name(FN, U_lX)
    der1_expr = mk_deriv1_expr(FN[1], U_lX[1], dFNdU_lX[1])
    dFNdg = mk_deriv1_name(FN, 'g')
    der1_expr += mk_deriv1_expr(FN[1], 'g', dFNdg[1])
    dFNdv_G = mk_deriv1_name(FN, 'v_G')
    der1_expr += mk_deriv1_expr(FN[1], 'v_G', dFNdv_G[1])

    # dvar: Dependent variables -- output variables and/or those to be diff
    # ivar: Independent variables -- input variables and/or diff w.r.t.
    # param: Parameters -- input variables not involved in differentiation
    outvar, invar = [dFNdU_lX, dFNdg, dFNdv_G], [U_lX, 'g', 'v_G', 'N', 'gam']

    # Call autogen to generate Maple code and rhoun Maple to generate 'C' code;
    # generate Fortran, Python, Matlab, etc code by changing 'C'
    autogen(expr + der1_expr, outvar, invar, 'euler1d', 'C',
            func='wall1d_flux_deriv', fname_mpl='tmp0.mpl', forceptr=['g', 'v_G', 'N', 'gam'])


'''
def wall1d_flux():

    #Generate 1D wall flux function wall1d_flux and its derivative wall1d_flux_deriv in euler1d.cc
    #F(U_x(v_G)/g) - U_x(v_G)/g *v_G = [0, p, p*v_G]
    #U_x are conservative variables in reference coordinate, g is mesh motion dx/dX, v_G is mesh velocity
    #:return:


    UX = ('UX', ['rhoX', 'rhouX', 'eX']) # 'UX' vector in reference coordinate
    U = ('U',['rho', 'rhou', 'e'])       #  U vector in deformed coordinate
    F = declare_vec('F', 3) # 'F' wall flux


    expr = ['rho = rhoX/g', 'rhou = rhouX/g', 'e = eX/g']

    expr += ['p = (gam-1)*(e - rhou*rhou/(2*rho))',
             'F1 = 0', 'F2 =  p*N','F3 = p*v_G*N']


    outvar, invar = [F], [UX, 'g', 'v_G', 'N', 'gam']

    autogen(expr, outvar, invar, 'euler1d', 'C', func='wall1d_flux', fname_mpl='tmp0.mpl', forceptr = ['g', 'v_G' , 'N', 'gamma'])

    # One derivative of F, G w.r.t. U
    dFdUX = mk_deriv1_name(F, UX)
    der1_expr = mk_deriv1_expr(F[1], UX[1], dFdUX[1])
    dFdg = mk_deriv1_name(F, 'g')
    der1_expr += mk_deriv1_expr(F[1], 'g', dFdg[1])
    dFdv_G = mk_deriv1_name(F, 'v_G')
    der1_expr += mk_deriv1_expr(F[1], 'v_G', dFdv_G[1])

    # dvar: Dependent variables -- output variables and/or those to be diff
    # ivar: Independent variables -- input variables and/or diff w.r.t.
    # param: Parameters -- input variables not involved in differentiation
    outvar, invar = [dFdUX, dFdg, dFdv_G], [UX, 'g', 'v_G','N', 'gam']

    # Call autogen to generate Maple code and rhoun Maple to generate 'C' code;
    # generate Fortran, Python, Matlab, etc code by changing 'C'
    autogen(expr+der1_expr, outvar, invar, 'euler1d', 'C',
            func='wall1d_flux_deriv', fname_mpl='tmp0.mpl', forceptr = ['g', 'v_G' ,'N', 'gam'])













def roe1d_flux():
    '''
        http://chimeracfd.com/programming/gryphon/fluxroe.html
        Generate 1D Roe flux function roe1d_flux and its derivative roe1d_flux_deriv in euler1d.cc
        F(U_x/g) - U_x/g *v_G
        U_lX,U_rX are conservative variables in reference coordinate, g is mesh motion dx/dX, v_G is mesh velocity
        F_ROE = 0.5*(F(U_l)- U_lv_G + F(U_r)- U_rv_G) - 0.5|A_ROE - v_GI|(U_r - U_l)
        :return:
    '''
    #
    # shift the velocity by v_G, compute flux

    U_lX = ('U_lX', ['rho_lX', 'rhou_lX', 'e_lX'])  # 'UX' conservative in reference domain
    U_l = ('U_l', ['rho_l', 'rhou_l', 'e_l'])  # 'U' conservative in deformed domain
    V_l = ('V_l', ['rho_l', 'u_l', 'p_l'])  # 'V' primitive in deformed domain
    expr = ['rho_l = rho_lX/g', 'rhou_l = rhou_lX/g', 'e_l = e_lX/g',
            'u_l = rhou_l/rho_l', 'p_l = (gam - 1)*(e_l - rhou_l*rhou_l/(2*rho_l))']

    # Define vector/matrix name and the name of its components
    U_rX = ('U_rX', ['rho_rX', 'rhou_rX', 'e_rX'])  # 'U' vector name; 'r', ..., 'e' components
    U_r = ('U_r', ['rho_r', 'rhou_r', 'e_r'])
    V_r = ('V_r', ['rho_r', 'u_r', 'p_r'])
    expr += ['rho_r = rho_rX/g', 'rhou_r = rhou_rX/g', 'e_r = e_rX/g', 'u_r = rhou_r/rho_r',
             'p_r = (gam - 1)*(e_r - rhou_r*rhou_r/(2*rho_r))']

    F_ave = declare_vec('F_ave', 3)  # 'F' vector name; 'F1', ..., 'F4' components

    expr += ['F_ave1 = 0.5*(rhou_r + rhou_l  - rho_l*v_G - rho_r*v_G)',
             'F_ave2 = 0.5*(rhou_r*rhou_r/rho_r + p_r  + rhou_l*rhou_l/rho_l + p_l - rhou_r*v_G - rhou_l*v_G)',
             'F_ave3 = 0.5*((e_l+p_l)*u_l + (e_r+p_r)*u_r - e_l*v_G - e_r*v_G)']

    dV = ('dV', ['drho', 'du', 'dp'])

    expr += ['drho = rho_r -rho_l', 'du = u_r - u_l', 'dp = p_r - p_l']
    expr += ['h_l = (e_l + p_l)/rho_l', 'h_r = (e_r + p_r)/rho_r']

    # rho average of sound speed
    expr += ['rho_rl = sqrt(rho_r *rho_l)',
             'u_rl = (sqrt(rho_l)*u_l + sqrt(rho_r)*u_r)/(sqrt(rho_l) + sqrt(rho_r))',
             'h_rl = (sqrt(rho_l)*h_l + sqrt(rho_r)*h_r)/(sqrt(rho_l) + sqrt(rho_r))']

    # roe average of sound speed
    expr += ['a_rl = sqrt((gam-1) *(h_rl - 0.5*u_rl*u_rl))']

    P_inv = ['1', 'u_rl', '0.5*u_rl*u_rl',
             'rho_rl/(2*a_rl)', 'rho_rl/(2*a_rl)*(u_rl + a_rl)', 'rho_rl/(2*a_rl)*(h_rl + u_rl *a_rl)',
             '-rho_rl/(2*a_rl)', '-rho_rl/(2*a_rl)*(u_rl - a_rl)', '-rho_rl/(2*a_rl)*(h_rl - u_rl *a_rl)']

    Lam_Abs = declare_vec('Lam_Abs', 3)
    expr += ['Lam_Abs1 = abs(u_rl - v_G)', 'Lam_Abs2 = abs(u_rl + a_rl - v_G)', 'Lam_Abs3 = abs(u_rl - a_rl - v_G)']

    dU = declare_vec('dU', 3)
    expr += ['dU1 = drho - dp/(a_rl*a_rl)', 'dU2 = du + dp/(rho_rl*a_rl)', 'dU3 =  du - dp/(rho_rl*a_rl)']

    mult = declare_vec('mult', 3)
    expr += ['mult1 = Lam_Abs1*dU1', 'mult2 = Lam_Abs2*dU2', 'mult3 =  Lam_Abs3*dU3']
    # F = 0.5(F_l + F_r - P_inv * Lam_Abs * dU)

    F = declare_vec('F', 3)  # 'F' Roe flux


    expr += gemv((3, 3), -0.5, P_inv, mult[1], 1.0, F_ave[1], F[1])

    FN = declare_vec('FN', 3)  # 'F' multiplies normal N

    expr += scal('N', F[1], FN[1])

    outvar, invar = [FN], [U_lX, U_rX, 'g', 'v_G', 'N', 'gam']

    autogen(expr, outvar, invar, 'euler1d', 'C', func='roe1d_flux', fname_mpl='tmp0.mpl',
            forceptr=['g', 'v_G', 'N', 'gam'])

    # One derivative of F, G w.r.t. U
    dFNdU_lX = mk_deriv1_name(FN, U_lX)
    der1_expr = mk_deriv1_expr(FN[1], U_lX[1], dFNdU_lX[1])
    dFNdU_rX = mk_deriv1_name(FN, U_rX)
    der1_expr += mk_deriv1_expr(FN[1], U_rX[1], dFNdU_rX[1])
    dFNdg = mk_deriv1_name(FN, 'g')
    der1_expr += mk_deriv1_expr(FN[1], 'g', dFNdg[1])
    dFNdv_G = mk_deriv1_name(FN, 'v_G')
    der1_expr += mk_deriv1_expr(FN[1], 'v_G', dFNdv_G[1])

    # dvar: Dependent variables -- output variables and/or those to be diff
    # ivar: Independent variables -- input variables and/or diff w.r.t.
    # param: Parameters -- input variables not involved in differentiation
    outvar, invar = [dFNdU_lX, dFNdU_rX, dFNdg, dFNdv_G], [U_lX, U_rX, 'g', 'v_G', 'N', 'gam']

    # Call autogen to generate Maple code and rhoun Maple to generate 'C' code;
    # generate Fortran, Python, Matlab, etc code by changing 'C'
    autogen(expr + der1_expr, outvar, invar, 'euler1d', 'C',
            func='roe1d_flux_deriv', fname_mpl='tmp0.mpl', forceptr=['g', 'v_G', 'N', 'gam'])








def pressure_load():

    #Generate 1D pressure load function pressure_load and its derivative pressure_load_deriv in euler1d.cc
    #p =  (gamma - 1)*(e - 0.5*rho*rho*u)
    #[rho, rhou, e] = [rhoX, rhouX, eX]/g
    #:return:


    UX = ('UX', ['rhoX', 'rhouX', 'eX']) # 'UX' vector in reference coordinate
    U = ('U',['rho', 'rhou', 'e'])       #  U vector in deformed coordinate


    expr = ['rho = rhoX/g', 'rhou = rhouX/g', 'e = eX/g']

    expr += ['p = (gam-1)*(e - rhou*rhou/(2*rho))']


    outvar, invar = ['p'], [UX, 'g', 'gam']

    autogen(expr, outvar, invar, 'euler1d', 'C', func='pressure_load', fname_mpl='tmp0.mpl',forceptr=['g', 'p', 'gam'])

    # One derivative of F, G w.r.t. U
    dpdUX = mk_deriv1_name('p', UX)
    der1_expr = mk_deriv1_expr('p', UX[1], dpdUX[1])
    dpdg = mk_deriv1_name('p', 'g')
    der1_expr += mk_deriv1_expr(['p'], 'g', dpdg[1])

    # dvar: Dependent variables -- output variables and/or those to be diff
    # ivar: Independent variables -- input variables and/or diff w.r.t.
    # param: Parameters -- input variables not involved in differentiation
    outvar, invar = [dpdUX, dpdg], [UX, 'g', 'gam']

    # Call autogen to generate Maple code and rhoun Maple to generate 'C' code;
    # generate Fortran, Python, Matlab, etc code by changing 'C'
    autogen(expr+der1_expr, outvar, invar, 'euler1d', 'C',
            func='pressure_load_deriv', fname_mpl='tmp0.mpl',forceptr=['g', 'p',  'dpdg', 'gam'])










if __name__ == "__main__":
    wall1d_flux()
    roe1d_flux()
    pressure_load()